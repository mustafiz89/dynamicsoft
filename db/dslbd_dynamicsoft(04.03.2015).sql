-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2015 at 02:57 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dslbd_dynamicsoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(3) NOT NULL,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_email` varchar(100) DEFAULT NULL,
  `admin_password` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'admin', 'admin@admin.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_board_member`
--

CREATE TABLE IF NOT EXISTS `tbl_board_member` (
`id` int(3) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `designation` varchar(80) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_board_member`
--

INSERT INTO `tbl_board_member` (`id`, `name`, `designation`, `image`) VALUES
(2, 'Md. Shahadat Hossain', 'Managing Director', 'image/board_member/administrator.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

CREATE TABLE IF NOT EXISTS `tbl_client` (
`id` int(4) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `weblink` varchar(100) NOT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_client`
--

INSERT INTO `tbl_client` (`id`, `image`, `weblink`, `description`) VALUES
(3, 'image/client/New-Bikroy.png', 'www.newbikroy.com', 'Core Banking: With a view towards modernizing service delivery to customers, Dhaka Bank Limited in 2003 selected FLEXCUBE as its core banking platform. The scope of work was: process and system study, assist the bank to identify the best available option for the technology platform.'),
(4, 'image/client/Bangladesh-Police.png', 'www.lgmc-bd.com', 'Core Banking: With a view towards modernizing service delivery to customers, Dhaka Bank Limited in 2003 selected FLEXCUBE as its core banking platform. The scope of work was: process and system study, assist the bank to identify the best available option for the technology platform.'),
(5, 'image/client/Nagar-Hospital1.png', 'www.dynamicsoftwareltd.com', 'Core Banking: With a view towards modernizing service delivery to customers, Dhaka Bank Limited in 2003 selected FLEXCUBE as its core banking platform. The scope of work was: process and system study, assist the bank to identify the best available option for the technology platform.'),
(6, 'image/client/Noapara-Group.png', 'www.dynamicsoftwareltd.com', 'Core Banking: With a view towards modernizing service delivery to customers, Dhaka Bank Limited in 2003 selected FLEXCUBE as its core banking platform. The scope of work was: process and system study, assist the bank to identify the best available option for the technology platform.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_history`
--

CREATE TABLE IF NOT EXISTS `tbl_company_history` (
`id` int(3) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_company_history`
--

INSERT INTO `tbl_company_history` (`id`, `title`, `description`) VALUES
(1, 'Brief History Of Dynamic Software Ltd', 'Dynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.\r\n\r\nDynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.\r\n\r\nDynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.\r\n\r\nDynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.\r\n\r\nDynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.\r\n\r\nDynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.\r\n\r\nDynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardware_product`
--

CREATE TABLE IF NOT EXISTS `tbl_hardware_product` (
`id` int(3) NOT NULL,
  `model` text,
  `specification` text NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `price` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_hardware_product`
--

INSERT INTO `tbl_hardware_product` (`id`, `model`, `specification`, `image`, `price`) VALUES
(2, 'Z-3100 Barcode Scanner <br/>\r\nModel: Z-3100 Barcode Scanner  <br/>\r\nBrand: ZEBEX <br/>\r\n', '→ Light Source: 617 nm visible red LED <br/>\r\n→ Optical System: Linear CCD array <br/> \r\n→ Microprocessor : 32 bit  <br/>\r\n→ Scan Rate : 330 scans per second  <br/>\r\n→ Print Contrast: Min. 30% @ UPC/EAN 100%<br/>\r\n→ Made in Taiwan  <br/>\r\n', 'image/hardware_product/Dynamic-S-Logo.png', '3500.00 BDT'),
(3, 'Z-3151 HS Barcode Scanner<br/>\r\nModel: Z-3151 HS Barcode Scanner <br/>\r\nBrand: ZEBEX<br/>\r\n', '→ Light Source: 650 nm visible laser diode (VLD)<br/>\r\n→ Microprocessor : 32 bit<br/>\r\n→ Scan Rate : 500 scans per second<br/>\r\n→ Print Contrast: 30% @ UPC/EAN 100%<br/>\r\n→ Made in Taiwan<br/>\r\n', 'image/hardware_product/1-186x200.jpg', '8500.00 BDT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_marketing_team`
--

CREATE TABLE IF NOT EXISTS `tbl_marketing_team` (
`id` int(4) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `division` varchar(100) NOT NULL,
  `contact` varchar(32) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_marketing_team`
--

INSERT INTO `tbl_marketing_team` (`id`, `name`, `designation`, `division`, `contact`, `email`, `image`) VALUES
(1, 'Md. Shariful Islam Raju ', 'Head Of Marketing ', 'dhaka.', '01553659875', 'raju@raju.com', 'image/marketing_team/administrator.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_portfolio`
--

CREATE TABLE IF NOT EXISTS `tbl_portfolio` (
`id` int(4) NOT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_portfolio`
--

INSERT INTO `tbl_portfolio` (`id`, `image`) VALUES
(2, 'image/portfolio/main_bg.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_programmer_team`
--

CREATE TABLE IF NOT EXISTS `tbl_programmer_team` (
`id` int(4) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `contact` varchar(32) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_programmer_team`
--

INSERT INTO `tbl_programmer_team` (`id`, `name`, `designation`, `contact`, `email`, `image`) VALUES
(1, 'Md. Farabi ', 'Jr. Software Enginner', '01553659875', 'farabi@nai.com', 'image/programmer_team/administrator.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recent_product`
--

CREATE TABLE IF NOT EXISTS `tbl_recent_product` (
`id` int(4) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `short_description` text,
  `long_description` text NOT NULL,
  `license` varchar(50) DEFAULT NULL,
  `product_size` varchar(50) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `used_technology` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_recent_product`
--

INSERT INTO `tbl_recent_product` (`id`, `name`, `short_description`, `long_description`, `license`, `product_size`, `price`, `used_technology`, `image`) VALUES
(3, 'Restaurant Management System &#40;RMS&#41;', 'ibtRM is a world class restaurant management system that are well tested and being used in top level restaurants in Bangladesh. Our restaurant management system is powered by ibtPOS, ibtSCM and it can be easily integrated with ibtHRM.', 'ibtRM is a world class restaurant management system that are well tested and being used in top level restaurants in Bangladesh. Our restaurant management system is powered by ibtPOS, ibtSCM and it can be easily integrated with ibtHRM.', 'Premium', '200MB', '25,000 BDT', 'C# and MSSQL Server', 'image/recent_product/011607pos1.jpg'),
(4, 'Hospital Management System &#40;HMS&#41;', 'HMS maintains a consulting service division which specializes in the implementation of our TimeControl timesheet system as well as Enterprise Project Management environments.\r\n\r\nWe are proud to count among our clients some of the world''s most recognizable organizations including: Acergy, Aecon, Rio Tinto, the Atlanta Airport, the City of Montreal, the County of San Mateo, Electromotive Diesel, EXFO, FT Services, John Deere, Kelly Services, Koch, the NHS, Organon, the Government of Quebec, Georgia Pacific, Parker Hannifin, Rolls Royce, Sobeys, Volvo Novabus and hundreds of others.', '', 'Premium', '300MB', '1,00,000 BDT', 'C# and MSSQL Server', 'image/recent_product/Hospital_Management_System_MainPage.jpg'),
(5, 'POS (Point Of Sale)', 'Your super shop may maintaining paper based Business Process for items, Sales, Purchase and inventory. Although, record keeping is not difficult but generating reports has become a major problem area in the current manual Business Process. We are a Leading POS Software Provider in Bangladesh,We provide POS Software which will keep records and generate daily, weekly or any periodical reports of items, Sales, Purchase, Accounting & Inventory which is normally prescribed for day to day activity. We deliver complete web based POS software with barcode generation and barcode integration by which you can manage multiple shop from a single point through internet.We have a good number of Client in Bangladesh who are Successfully operate their Shop, Chain Shop through online or offline.', 'Your super shop may maintaining paper based Business Process for items, Sales, Purchase and inventory. Although, record keeping is not difficult but generating reports has become a major problem area in the current manual Business Process. We are a Leading POS Software Provider in Bangladesh,We provide POS Software which will keep records and generate daily, weekly or any periodical reports of items, Sales, Purchase, Accounting & Inventory which is normally prescribed for day to day activity. We deliver complete web based POS software with barcode generation and barcode integration by which you can manage multiple shop from a single point through internet.We have a good number of Client in Bangladesh who are Successfully operate their Shop, Chain Shop through online or offline.', 'Premium', '200MB', '40,000 BDT', 'C# and MSSQL Server', 'image/recent_product/index.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE IF NOT EXISTS `tbl_service` (
`id` int(4) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `short_description` text,
  `long_description` text,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`id`, `title`, `short_description`, `long_description`, `image`) VALUES
(2, 'Web Design', '2222222222222', 'aqqqqqq', 'image/service/dynamic1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_technology`
--

CREATE TABLE IF NOT EXISTS `tbl_technology` (
`id` int(3) NOT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_technology`
--

INSERT INTO `tbl_technology` (`id`, `description`) VALUES
(1, 'Dynamic software ltd has always stood for a deep understanding of the technologies it works with. The company has regularly invested in understanding and using cutting edge technologies that it sees as relevant to building cost effective solutions for its clients. Its research groups focus on assimilating technology, creating frameworks and processes that help design and build highly scaleable and performance driven applications for the customers. We optimally utilize technology as the perfect means to an end. The current focus area includes but is not limited to .NET Technologies, XML, SQL Server, and MySQL.\r\n                                   Recommending the technology for a solution requires and in-depth understanding of the benefits of any and all technologies that we cover. The choices are more limited sometimes although. However, we''ve deployed some interesting solutions across all platforms . \r\n                                  ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_upcoming_product`
--

CREATE TABLE IF NOT EXISTS `tbl_upcoming_product` (
`id` int(3) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `possible_price` varchar(32) NOT NULL,
  `used_tools` text,
  `image` varchar(100) DEFAULT NULL,
  `release_date` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_upcoming_product`
--

INSERT INTO `tbl_upcoming_product` (`id`, `name`, `description`, `possible_price`, `used_tools`, `image`, `release_date`) VALUES
(1, 'Hospital Management System &#40;HMS&#41;', 'Write a description about Hospital Management System', '1,00,000 BDT', '1. C# \r\n2. MS Sql Server 2008 \r\n3.Crystal Report', 'image/upcoming_product/main_bg.jpg', '03/06/2015'),
(2, 'Restaurant Management System &#40;RMS&#41; ', 'Write a description about Your Restaurant Management System &#40;RMS&#41; ', '25,000 BDT', '<li>C#</li>\r\n<li>SQL Server 2008</li>\r\n<li>Crystal Report</li>', 'image/upcoming_product/10.jpg', '07/07/2015');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_welcome_message`
--

CREATE TABLE IF NOT EXISTS `tbl_welcome_message` (
`id` int(3) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_welcome_message`
--

INSERT INTO `tbl_welcome_message` (`id`, `title`, `description`) VALUES
(1, 'Welcome To Dynamic Software Ltd', 'Dynamic Software Company delivers industry-standard quality software outsourcing service and ensures free prototype development (for new outsourcing clients) along with management consulting service (from domain expert) with every software development and also guarantee 24/7 after sales service support and a dedicated project management team in a very reasonable cost.\r\n<p>\r\nDynamic ensures Proper Documentation, Bug free Application, Performance Tuning Service, User friendly Interface, Secured Design, Multilingual support for any software application and W3 Standard, Search Engine Optimization (SEO), Control Panel Facility in any web application and portal.</p>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_board_member`
--
ALTER TABLE `tbl_board_member`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_client`
--
ALTER TABLE `tbl_client`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_company_history`
--
ALTER TABLE `tbl_company_history`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardware_product`
--
ALTER TABLE `tbl_hardware_product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_marketing_team`
--
ALTER TABLE `tbl_marketing_team`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_programmer_team`
--
ALTER TABLE `tbl_programmer_team`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recent_product`
--
ALTER TABLE `tbl_recent_product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_service`
--
ALTER TABLE `tbl_service`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_technology`
--
ALTER TABLE `tbl_technology`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_upcoming_product`
--
ALTER TABLE `tbl_upcoming_product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_board_member`
--
ALTER TABLE `tbl_board_member`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_client`
--
ALTER TABLE `tbl_client`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_company_history`
--
ALTER TABLE `tbl_company_history`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_hardware_product`
--
ALTER TABLE `tbl_hardware_product`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_marketing_team`
--
ALTER TABLE `tbl_marketing_team`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_programmer_team`
--
ALTER TABLE `tbl_programmer_team`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_recent_product`
--
ALTER TABLE `tbl_recent_product`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_service`
--
ALTER TABLE `tbl_service`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_technology`
--
ALTER TABLE `tbl_technology`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_upcoming_product`
--
ALTER TABLE `tbl_upcoming_product`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
