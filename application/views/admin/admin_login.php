<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Dynamic Software Ltd</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="<?= base_url() ?>style/bootstrap.css" rel="stylesheet"/>
    <!-- FONTAWESOME STYLES-->
    <link href="<?= base_url() ?>style/font-awesome.css" rel="stylesheet"/>
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>

</head>
<body style="background-color: #E2E2E2;">
<div class="container">
    <div class="row text-center " style="padding-top:100px;">
        <div class="col-md-12">
            <!--            <img src="assets/img/logo-invoice.png"/>-->
            <h1>Dynamic Software Ltd</h1>
            <h5>Please login with your Username and Password.</h5>
        </div>
    </div>
    <div class="row ">

        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="text-center">
                <?php
                $msg = $this->session->userdata('message');
                if ($msg) {

                    echo '<h3 class="label label-success">';
                    echo $msg;
                    echo '</h3>';
                    $this->session->unset_userdata('message');
                }
                ?>

                <?php
                $msg = $this->session->userdata('exception');
                if ($msg) {

                    echo '<h3 class="label label-danger">';
                    echo $msg;
                    echo '</h3>';
                    $this->session->unset_userdata('exception');
                }
                ?>
            </div>

            <div class="panel-body">
                <form role="form" action="<?= base_url(); ?>login/admin_login_check" method="post">
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                        <input type="text" class="form-control" placeholder="Your email" name="admin_email"/>
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" class="form-control" placeholder="Your Password" name="admin_password"/>
                    </div>
                    <!--                    <div class="form-group">-->
                    <!--                        <label class="checkbox-inline">-->
                    <!--                            <input type="checkbox"/> Remember me-->
                    <!--                        </label>-->
                    <!--                        <span class="pull-right">-->
                    <!--                               <a href="index.html">Forget password ? </a>-->
                    <!--                        </span>-->
                    <!--                    </div>-->

                    <button type="submit" class="btn btn-primary btn-block ">Sign In</button>
                    <hr/>
                    Not register ? <a href="#">click here </a>
                </form>
            </div>

        </div>


    </div>
</div>

</body>
</html>