<div class="row">
        <div class="col-md-9" id="addform">
        <div class="widget">

            <div class="widget-head">
                <div class="pull-left">Technology Information</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content">
                <div class="padd">
                    <p style="color: #330033; text-align: center; font-size: 16px;">
                        <?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </p>
                    <form action="<?php echo base_url() ?>administrator/Update_technology" method="post" >
                        <fieldset>
                            <div class="form-group">
                                  <label class="col-lg-3"> Technology Description</label>
                                  <div class="col-lg-9">
                                     <textarea class="form-control" required rows="15" placeholder="Description" name="description" ><?php echo $get_data->description;?></textarea><br/>
                                    <input type="hidden" class="form-control placeholder" name="id" value="<?php echo $get_data->id;?>"/><br/>

                                  </div>
                              </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" class="btn btn-success btn-lg">Update Changes</button>
                                   
                                </div>
                            </div> 

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</div>