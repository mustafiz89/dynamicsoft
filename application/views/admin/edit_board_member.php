
<div class="row">
    
         <div class="col-md-7" id="addform">
        <div class="widget">

            <div class="widget-head">
                <div class="pull-left">Edit Client Information</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content">
                <div class="padd">
                    <p style="color: #330033; text-align: center; font-size: 16px;">
                        <?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </p>
                    <form action="<?php echo base_url() ?>about/update_board_member" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group">
                                  <label class="col-lg-3">Name</label>
                                  <div class="col-lg-9">
                                      <input type="text" class="form-control placeholder" required id="personName" placeholder="Name" name="name" value="<?php echo $get_data->name;?>"/><br/>

                                  </div>
                              </div>
                            <div class="form-group">
                                  <label class="col-lg-3">Designation</label>
                                  <div class="col-lg-9">
                                    <input type="text" class="form-control placeholder" required id="personName" placeholder="Designation" name="designation" value="<?php echo $get_data->designation;?>"/><br/>

                                  </div>
                              </div>
                            <div class="form-group">
                                <label class="col-lg-3" for="name">Existing Image</label>
                                <div class="col-lg-8"> 
                                    <img src="<?php echo base_url().$get_data->image;?>" width="100" height="100" /><br/><br/>
                                    <input type="hidden" class="form-control placeholder" name="id" value="<?php echo $get_data->id;?>"/><br/>

                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3" for="name">New Image</label>
                                <div class="col-lg-8"> 
                                   
                                    <input type="file" name="image"  accept="image/*"/><span style="color: #009999;">(Image size should not exceed 3Mb and 1200*900 pixel)</span><br/><br/>
                                       
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" class="btn btn-success btn-lg">Update Changes</button>
<!--                                    <button  type="reset" class="btn btn-primary btn-lg">Clear</button>-->
                                </div>
                            </div> 

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

