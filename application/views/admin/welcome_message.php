<div class="row">
    <div class="col-md-9">
        <div class="widget">

            <div class="widget-head">
                <div class="pull-left">Update Welcome Information</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content">
                <div class="padd">
                    <p style="text-align: center; color: green; font-size: 16px;">
                        <?php 
                        $msg=$this->session->userdata('message');
                                if($msg)
                                {
                                   echo $msg; 
                                   $this->session->unset_userdata('message');
                                }
                        ?>
                    </p>
                    <form action="<?php echo base_url()?>administrator/update_welcome_message" method="post">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-lg-2" for="name">Title</label>
                                <div class="col-lg-10">
                                    <input type="text"  class="form-control" name="title" required value="<?php echo $get_data->title;?>"/><br/>
                                    <input type="hidden"  class="form-control" name="id" value="<?php echo $get_data->id;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Description</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" rows="15"  required name="description"><?php echo $get_data->description;?></textarea><br/>
                                </div>
                            </div> 
                            
                            <div class="form-group">
                                <label class="col-lg-2 control-label"></label>
                                <div class="col-lg-10">
                                     <button type="submit" class="btn btn-success btn-lg">Update Changes</button>
<!--                                     <button  type="reset" class="btn btn-primary btn-lg">Clear</button>-->
                                </div>
                            </div> 
                           
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
