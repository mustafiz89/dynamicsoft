<div class="row">
     <div class="col-md-7" id="addform">
        <div class="widget">

            <div class="widget-head">
                <div class="pull-left">Add Product Information</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content">
                <div class="padd">
                    <p style="color: #330033; text-align: center; font-size: 16px;">
                        <?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </p>
                    <form action="<?php echo base_url() ?>product/update_recent_product" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group">
                                  <label class="col-lg-3">Product Name</label>
                                  <div class="col-lg-9">
                                      <input type="text" class="form-control " required id="personName" placeholder="Name" name="name" value="<?php echo $get_data->name;?>"/>
                                       <input type="hidden" class="form-control " required id="personName" placeholder="Name" name="id" value="<?php echo $get_data->id;?>"/><br/>

                                  </div>
                              </div>
                            
                            <div class="form-group">
                                  <label class="col-lg-3">Short Description</label>
                                  <div class="col-lg-9">
                                      <textarea class="form-control" required rows="7" maxlength="600" placeholder="Product Description" name="short_description"><?php echo $get_data->short_description;?></textarea><br/>

                                  </div>
                              </div>
                             <div class="form-group">
                                  <label class="col-lg-3">Long Description</label>
                                  <div class="col-lg-9">
                                      <textarea class="form-control" required rows="20" placeholder="Product Description" name="long_description"><?php echo $get_data->long_description;?></textarea><br/>

                                  </div>
                              </div>
                           
                            <div class="form-group">
                                  <label class="col-lg-3">License</label>
                                  <div class="col-lg-9">
                                    <input type="text" class="form-control " required id="personName" placeholder="License" name="license" value="<?php echo $get_data->license;?>"/><br/>

                                  </div>
                              </div>
                             <div class="form-group">
                                  <label class="col-lg-3">Product Size</label>
                                  <div class="col-lg-9">
                                      <input type="text" class="form-control" required id="personName" placeholder="Product Size" name="product_size" value="<?php echo $get_data->product_size;?>"/><br/>

                                  </div>
                              </div>
                             <div class="form-group">
                                  <label class="col-lg-3">Price</label>
                                  <div class="col-lg-9">
                                      <input type="text" class="form-control" required id="personName" placeholder="Price" name="price" value="<?php echo $get_data->price;?>"/><br/>

                                  </div>
                              </div>
                            <div class="form-group">
                                  <label class="col-lg-3">Used Technology</label>
                                  <div class="col-lg-9">
                                      <input type="text" class="form-control" required id="personName" placeholder="Technology Used In" name="used_technology" value="<?php echo $get_data->used_technology;?>"/><br/>

                                  </div>
                              </div>
                            <div class="form-group">
                                <label class="col-lg-3" for="name">Existing Image</label>
                                <div class="col-lg-8"> 
                                    <img src="<?php echo base_url().$get_data->image;?>" width="100" height="100" /><br/><br/>
                                    <input type="hidden" class="form-control placeholder" name="id" value="<?php echo $get_data->id;?>"/><br/>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3" for="name">Image</label>
                                <div class="col-lg-8"> 
                                   
                                    <input type="file" name="image"  accept="image/*"/><span style="color: #009999;">(Image size should not exceed 3Mb and 1200*900 pixel)</span><br/><br/>
                                       
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" class="btn btn-success btn-lg">Update Changes</button>
                                    <button  type="reset" class="btn btn-primary btn-lg">Clear</button>
                                </div>
                            </div> 

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    
</div>