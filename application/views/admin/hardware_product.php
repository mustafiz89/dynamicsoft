<div class="row">
    
         <div class="col-md-5" id="addform">
        <div class="widget">

            <div class="widget-head">
                <div class="pull-left">Add Hardware Information</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content">
                <div class="padd">
                    <p style="color: #330033; text-align: center; font-size: 16px;">
                        <?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </p>
                    <form action="<?php echo base_url() ?>product/add_hardware_product" method="post" enctype="multipart/form-data">
                        <fieldset>
                           
                             <div class="form-group">
                                  <label class="col-lg-3">Price</label>
                                  <div class="col-lg-9">
                                      <input type="text" class="form-control placeholder" required id="personName" placeholder="Price" name="price" value=""/><br/>

                                  </div>
                              </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Model</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" rows="6"  maxlength="385" required name="model" placeholder="Model Description"></textarea><br/>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Specification</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" rows="8"  required name="specification" placeholder="Product Specification"></textarea><br/>
                                </div>
                            </div> 
                             <div class="form-group">
                                <label class="col-lg-3" for="name">Image</label>
                                <div class="col-lg-8"> 
                                   
                                    <input type="file" name="image" required="file" accept="image/*"/><span style="color: #009999;">(Image size should not exceed 1Mb and 800*600 pixel)</span><br/><br/>
                                       
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" class="btn btn-success btn-lg">Add Changes</button>
                                    <button  type="reset" class="btn btn-primary btn-lg">Clear</button>
                                </div>
                            </div> 

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-7">
        <div class="widget">

            <div class="widget-head">
                <div class="pull-left">Manage Hardware Information</div>
                <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="widget-content">
                <div class="padd">
                    <p style="text-align: center; color: green; font-size: 16px;">
                        <?php
                        $msg = $this->session->userdata('d_message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('d_message');
                        }
                        ?>
                    </p>
                    <?php
                    if(count($get_all_data)!=0)
                    {
                    ?>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Information</th>
                                <th>Control</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=0;
                            foreach($get_all_data as $v_data)
                            {
                                $i+=1;                                
                            ?>
                       

                            <tr>
                                <td><?php echo $i;?></td>
                                <td><img src="<?php echo base_url().$v_data->image;?>" width="80" height="50"/></td>
                                <td>
                                    <p>Model No: <?php echo $v_data->model;?></p>
                                    <p>Price: <?php echo $v_data->price;?></p>
                                </td>
                                    
                                
                                <td>

                                   
                                    <a href="<?php echo base_url();?>product/edit_hardware_product/<?php echo $v_data->id;?>"><button class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-pencil" ></i></button></a>
                                    <a href="<?php echo base_url();?>product/delete_hardware_product/<?php echo $v_data->id;?>"><button class="btn btn-xs btn-danger" title="Delete" onclick="return check_delete();"><i class="fa fa-times"></i> </button>
</a>
                                 

                                </td>
                            </tr>
                            <?php                             
                            }
                            ?>

                        
                        </tbody>
                        
                    </table>
                    <?php 
                           
                    }
                    ?>
                    <div class="widget-foot">

                     
                        <ul class="pagination pull-right">
                         
                            <li><?php echo $this->pagination->create_links(); ?></li>
                          
                        </ul>
                     
                      <div class="clearfix"></div> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

