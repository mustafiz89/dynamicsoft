<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <!-- Title and other stuffs -->
        <title>Dashboard </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">


        <!-- Stylesheets -->
        <link href="<?php echo base_url();?>style/bootstrap.css" rel="stylesheet">
        <!-- Font awesome icon -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/font-awesome.css"> 
        <!-- jQuery UI -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/jquery-ui-1.9.2.custom.min.css"> 
        <!-- Calendar -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/fullcalendar.css">
        <!-- prettyPhoto -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/prettyPhoto.css">  
        <!-- Star rating -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/rateit.css">
        <!-- Date picker -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/bootstrap-datetimepicker.min.css">
        <!-- CLEditor -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/jquery.cleditor.css"> 
        <!-- Uniform -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/uniform.default.html"> 
        <!-- Uniform -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/daterangepicker-bs3.css" />
        <!-- Bootstrap toggle -->
        <link rel="stylesheet" href="<?php echo base_url();?>style/bootstrap-switch.css">
        <!-- Main stylesheet -->
        <link href="<?php echo base_url();?>style/style.css" rel="stylesheet">
        <!-- Widgets stylesheet -->
        <link href="<?php echo base_url();?>style/widgets.css" rel="stylesheet">   
        
        
        <!-- Gritter Notifications stylesheet -->
        <!--<link href="<?php echo base_url();?>style/jquery.gritter.css" rel="stylesheet">-->   

        <!-- HTML5 Support for IE -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url() ?>js/html5shim.js"></script>
        <![endif]-->

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>image/favicon/favicon.ico">
        <script>
         function check_delete()
            {
                var chk = confirm('Are you sure to delete this!');
                if (chk)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        
        </script>
    </head>

    <body>
        <header>
            <div class="navbar navbar-fixed-top bs-docs-nav" role="banner">

                <div class="container">
                    <!-- Menu button for smallar screens -->
                    <div class="navbar-header">
                        <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse"><span>Menu</span></button>
                        <a href="#" class="pull-left menubutton hidden-xs"><i class="fa fa-bars"></i></a>
                        <!-- Site name for smallar screens -->
                        <a href="<?php echo base_url();?>administrator/dashboard" class="navbar-brand">Dynamic<span class="bold">Software</span></a>
                    </div>

                    <!-- Navigation starts -->
                    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">         

                        <!-- Links -->
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown pull-right user-data">            
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <img src="<?php echo base_url();?>image/user1.png"><?php echo $this->session->userdata('admin_name')?><b class="caret"></b>              
                                </a>

                                <!-- Dropdown menu -->
                                <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                                    <li><a href="#"><i class="fa fa-cogs"></i> Settings</a></li>
                                    <li><a href="<?php echo base_url(); ?>administrator/logout"><i class="fa fa-key"></i> Logout</a></li>
                                </ul>
                            </li>


                        </ul>
                    </nav>

                </div>
            </div>
        </header>
        <!-- Main content starts -->

        <div class="content">

            <!-- Sidebar -->
            <div class="sidebar">
                <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
                <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->
                <ul id="nav">
                    <!-- Main menu with font awesome icon -->
                    <li><a href="<?php echo base_url();?>administrator/index.aspx" class="has_sub"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                    <li><a href="<?php echo base_url();?>administrator/welcome_message.aspx"><i class="fa fa-edit"></i> <span>Welcome Message</span></a></li> 
<!--                    <li><a href="<?php echo base_url();?>administrator/company_history.aspx"><i class="fa fa-edit"></i> <span>Company History</span></a></li> -->

                    <li class="has_sub"><a href="#"><i class="fa fa-edit"></i> <span>About Us</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                        <ul>
                             <li><a href="<?php echo base_url();?>administrator/company_history.aspx"><i class="fa fa-arrow-right"></i> <span>Company History</span></a></li> 
                             <li><a href="<?php echo base_url()?>about/index.aspx"><i class="fa fa-arrow-right"></i> <span>Board Member's</span></a></li>
                             <li><a href="<?php echo base_url()?>about/manage_programmer_team.aspx"><i class="fa fa-arrow-right"></i> <span>Programmer Team</span></a></li>
                             <li><a href="<?php echo base_url()?>about/manage_marketing_team.aspx"><i class="fa fa-arrow-right"></i> <span>Marketing Team</span></a></li>

                        </ul>
                    </li> 

                    <li class="has_sub"><a href="#"><i class="fa fa-edit"></i> <span>Product</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                        <ul>
                             <li><a href="<?php echo base_url();?>product/manage_recent_product.aspx"><i class="fa fa-arrow-right"></i> <span>Recent Product</span></a></li> 
                             <li><a href="<?php echo base_url()?>product/manage_upcoming_product.aspx"><i class="fa fa-arrow-right"></i> <span>Upcoming Product</span></a></li> 
                             <li><a href="<?php echo base_url()?>product/manage_hardware_product.aspx"><i class="fa fa-arrow-right"></i> <span>Hardware Product</span></a></li> 

                        </ul>
                    </li> 
                    <li><a href="<?php echo base_url()?>service/manage_service.aspx"><i class="fa fa-edit"></i> <span>Service's</span></a></li> 
                    <li><a href="<?php echo base_url()?>administrator/manage_technology.aspx"><i class="fa fa-edit"></i> <span>Technology</span></a></li> 

                    
                    
                    <li><a href="<?php echo base_url();?>client/index.aspx"><i class="fa fa-edit"></i> <span>Our Client</span></a></li>                             
                    <li><a href="<?php echo base_url();?>administrator/manage_portfolio.aspx"><i class="fa fa-edit"></i> <span>Portfolio</span></a></li>                             
                    
<!--                    <li class="has_sub"><a href="#"><i class="fa fa-list-alt"></i> <span>Widgets</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                        <ul>
                            <li><a href="widgets1.html">Widgets #1</a></li>
                            <li><a href="widgets2.html">Widgets #2</a></li>
                            <li><a href="widgets3.html">Widgets #3</a></li>
                        </ul>
                    </li>  
                    
                    <li class="has_sub"><a href="#"><i class="fa fa-heart"></i> <span>3 Level Menu</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                        <ul>
                            <li><a href="#"><i class="fa fa-bookmark"></i> Subitem 1</a></li>
                            <li class="has_sub"><a href="#"><i class="fa fa-glass"></i> Subitem 2 <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                                <ul>
                                    <li><a href="#"><i class="fa fa-bell"></i> Subitem 1</a></li>
                                    <li><a href="#"><i class="fa fa-camera"></i> Subitem 2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="has_sub"><a href="#"><i class="fa fa-folder"></i> <span>4 Level Menu</span> <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                        <ul>
                            <li><a href="#"><i class="fa fa-thumb-tack"></i> Subitem 1</a></li>
                            <li class="has_sub"><a href="#"><i class="fa fa-thumbs-up"></i> Subitem 2 <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                                <ul>
                                    <li><a href="#"><i class="fa fa-trophy"></i> Subitem 1</a></li>
                                    <li class="has_sub"><a href="#"><i class="fa fa-share"></i> Subitem 2 <span class="pull-right"><i class="fa fa-chevron-left"></i></span></a>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-microphone"></i> Subitem 1</a></li>
                                            <li><a href="#"><i class="fa fa-phone"></i> Subitem 2</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>-->
                </ul>
            </div>
            <!-- Sidebar ends -->

            <!-- Main bar -->
            <div class="mainbar">

                <!-- Page heading -->
                <div class="page-head">
                    <h2 class="pull-left"><?php echo $title;?></h2>
<!--                    <div class="pull-right">
                        <div id="reportrange" class="pull-right">
                            <i class="fa fa-calendar"></i>
                            <span></span> <b class="caret"></b>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                    <!-- Breadcrumb -->
                    <div class="bread-crumb">
                        <a href="<?php echo base_url();?>administrator/dashboard"><i class="fa fa-home"></i> Home</a> 
                        <!-- Divider -->
                        <span class="divider">/</span> 
                        <a href="#" class="bread-current"><?php echo $title;?></a>
                    </div>

                    <div class="clearfix"></div>

                </div>
                <!-- Page heading ends -->
                <!-- Matter -->

                <div class="matter">
                    <div class="container">

                            
                        
                        <?php echo $admin_mid_content;?>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </div>
                </div>

                <!-- Matter ends -->

            </div>

            <!-- Mainbar ends -->
            <div class="clearfix"></div>

        </div>
        <!-- Content ends -->

        <!-- Footer starts -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Copyright info -->
                        <p class="copy">Copyright &copy; 2015 | <a href="#">Dynamic Software Ltd</a> </p>
                    </div>
                </div>
            </div>
        </footer> 	

        <!-- Footer ends -->

        <!-- Scroll to top -->
        <span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 

        <!-- JS -->
        <script src="<?php echo base_url() ?>js/jquery.js"></script> <!-- jQuery -->
        <script src="<?php echo base_url() ?>js/bootstrap.js"></script> <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>js/jquery-ui-1.9.2.custom.min.js"></script> <!-- jQuery UI -->
        <script src="<?php echo base_url() ?>js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
        <script src="<?php echo base_url() ?>js/jquery.rateit.min.js"></script> <!-- RateIt - Star rating -->
        <script src="<?php echo base_url() ?>js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->
        <script src="<?php echo base_url() ?>js/jquery.validationEngine.js"></script> <!-- jQuery Validation Engine -->
        <!-- Morris JS -->
        <script src="<?php echo base_url() ?>js/raphael-min.js"></script>
        <script src="<?php echo base_url() ?>js/morris.min.js"></script>

        <!-- jQuery Flot -->
        <script src="<?php echo base_url() ?>js/excanvas.min.js"></script>
        <script src="<?php echo base_url() ?>js/jquery.flot.js"></script>
        <script src="<?php echo base_url() ?>js/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url() ?>js/jquery.flot.pie.js"></script>
        <script src="<?php echo base_url() ?>js/jquery.flot.stack.js"></script>

        <!-- jQuery Notification - Noty -->
        <script src="<?php echo base_url() ?>js/jquery.noty.js"></script> <!-- jQuery Notify -->
        <script src="<?php echo base_url() ?>js/themes/default.js"></script> <!-- jQuery Notify -->
        <script src="<?php echo base_url() ?>js/layouts/bottom.js"></script> <!-- jQuery Notify -->
        <script src="<?php echo base_url() ?>js/layouts/topRight.js"></script> <!-- jQuery Notify -->
        <script src="<?php echo base_url() ?>js/layouts/top.js"></script> <!-- jQuery Notify -->
        <!-- jQuery Notification ends -->

        <!-- Daterangepicker -->
        <script src="<?php echo base_url() ?>js/moment.min.js"></script>
<!--        <script src="<?php echo base_url() ?>js/daterangepicker.js"></script>-->

        <script src="<?php echo base_url() ?>js/sparklines.js"></script> <!-- Sparklines -->
        <!--<script src="<?php echo base_url() ?>js/jquery.gritter.min.js"></script>  jQuery Gritter -->
        <script src="<?php echo base_url() ?>js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
        <script src="<?php echo base_url() ?>js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
        <script src="<?php echo base_url() ?>js/jquery.uniform.min.html"></script> <!-- jQuery Uniform -->
        <script src="<?php echo base_url() ?>js/jquery.slimscroll.min.js"></script> <!-- jQuery SlimScroll -->
        <script src="<?php echo base_url() ?>js/bootstrap-switch.min.js"></script> <!-- Bootstrap Toggle -->
        <script src="<?php echo base_url() ?>js/jquery.maskedinput.min.js"></script> <!-- jQuery Masked Input -->
        <script src="<?php echo base_url() ?>js/dropzone.js"></script> <!-- jQuery Dropzone -->
        <script src="<?php echo base_url() ?>js/filter.js"></script> <!-- Filter for support page -->
        <script src="<?php echo base_url() ?>js/custom.js"></script> <!-- Custom codes -->
        <script src="<?php echo base_url() ?>js/charts.js"></script> <!-- Charts & Graphs -->
        <script src="<?php echo base_url() ?>js/jquery.icheck.min.js"></script> <!-- jQuery iCheck -->

<!--        <script src="<?php echo base_url() ?>js/index.js"></script>  Index Javascripts -->
<script>
$(document).ready(function(){   
  $(".validate").validationEngine();
  $.mask.definitions['^']='[1-9]';
    $('#inputDate').mask('99/99/9999');
    });

</script>

       
    </body>

    <!-- Mirrored from r209.com/templates/moodstrap/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Feb 2015 12:45:52 GMT -->
</html>