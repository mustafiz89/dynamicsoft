
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            
            <!-- Gallery starts -->
            
            <div class="gallery">
               <div class="row">
                  <div class="col-md-12">
                  
                     <!-- Gallery hero -->
                     <div class="hero">
                        <!-- Title. Don't forget the <span> tag -->
                        <h3><span><?php echo $title;?></span></h3>
                        <!-- para -->
                        <p></p>
                     </div>
                     <!-- Gallery -->
                     
                    <div id="gallery">
                        <?php 
                        foreach($get_all_data as $v_data)
                        {
                        
                        ?>
                        <a href="<?php echo base_url().$v_data->image;?>" class="prettyphoto">
                           <img src="<?php echo base_url().$v_data->image;?>" alt=""/>
                       </a>
                       <?php 
                        }
                       ?>
                    </div>
                     
                  </div>
               </div>
            </div>
            
            
            <!-- gallery ends -->
            
         </div>
      </div>
   </div>
</div>   
