<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            
            <!-- Products starts -->
            
            <div class="products">
               <div class="row">
                  <div class="col-md-12">
                  
                     <!-- Products hero -->
                     <div class="hero">
                        <!-- Title. Don't forget the <span> tag -->
                        <h3><span>Products</span></h3>
                        <!-- para -->
                        <p></p></div>
                     
                     <!-- Products -->
                     
                     <!-- List of products list. -->
                     <?php
                     foreach($get_all_data as $v_data)
                     {
                     ?>
                     <div class="prod">
                        <div class="row">
                           <div class="col-md-7 col-sm-8">
                              <!-- Product title -->
                              <h3><?php echo $v_data->name?></h3>
                              <!-- Product para -->
                              <p><?php echo $v_data->short_description?>...<a href="<?php echo base_url();?>welcome/product_details/<?php echo $v_data->id?>">Read More</a></p>
                              <div class="pimg">
                                 <a href="#"><img src="<?php echo base_url().$v_data->image;?>" alt="" height="300" width="550" /></a>
                              </div>
                           </div>                        
                           <div class="col-md-5 col-sm-4"> 
                              <!-- Product details -->
                              <div class="pdetails">
                                 <div class="ptable">
                                    <!-- Product details with font awesome icon. Don't forget the span class "pull-right". -->
                                    <div class="pline"><i class="fa fa-gift"></i> Used Technology: <span class="pull-right"><?php echo $v_data->used_technology?></span></div>
                                    <div class="pline"><i class="fa fa-cloud"></i> License <span class="pull-right"><?php echo $v_data->license?></span></div>
                                    <div class="pline"><i class="fa fa-bullhorn"></i> Product Size <span class="pull-right"><?php echo $v_data->product_size;?></span></div>
                                    <div class="pline"><i class="fa fa-truck"></i> Price <span class="pull-right"><?php echo $v_data->price?></span></div>
                                    <div class="clearfix"></div>
                                 </div>
                                 <!-- Buttons -->
                                 <div class="button center"><a href="#">Try Now</a></div>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <?php } ?>
                     <div class="col-md-12 center">
                         <div class="pagination pagination-sm">
                             <li><?php echo $this->pagination->create_links(); ?></li>

                         </div>  
                     </div>
                     
                    
                  </div>
               </div>
            </div>
            
            
            <!-- Service ends -->
            
            
         </div>
      </div>
   </div>
</div>   
