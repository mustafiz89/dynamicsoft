<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- Projects starts -->

                <div class="project">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- Projects hero -->
                            <div class="hero">
                               <!-- Title. Don't forget the <span> tag -->
                                <h3><span>Our Upcoming Product</span></h3>
                                <!-- para -->
                                <p></p>
                            </div>

                            <!-- Projects -->

                            <!-- List of products list. -->
                            <?php
                            foreach ($get_all_data as $v_data) {
                                ?>
                                <div class="prod">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-8">
                                            <h3><?php echo $v_data->name ?></h3>
                                            <hr />
                                            <!-- Project slideshow -->

                                            <div class="pimg">
                                                <a href="<?php echo base_url() . $v_data->image; ?>"><img src="<?php echo base_url() . $v_data->image; ?>" alt="" height="300" width="450" /></a>
                                            </div>
                                            <!-- Carousel nav -->
                                        </div>  


                                        <div class="col-md-6 col-sm-4">
                                            <!-- Project details -->
                                            <div class="pdetails">
                                                  <div class="prdetails">
                                                <!-- Project title -->
                                                <h2>Product Details</h2>
                                                <!-- Project para -->
                                                <p><?php echo $v_data->description; ?></p>
                                                
                                                <h6>Tools Used</h6>

                                                <ul>
                                                    <!--<li><i class="fa fa-angle-right"></i> HTML5/CSS3/jQuery</li>-->
                                                    <?php echo $v_data->used_tools ?>

                                                </ul>
                                                <h6>Possible Price: <?php echo $v_data->possible_price?></h6>
                                                <h6>Release Date: <?php echo $v_data->release_date?></h6>
                                                <!-- Buttons -->
                                                <!--<div class="button"><a href="#"><i class="fa fa-cloud"></i> Launch Project</a> </div>-->
                                            </div>
                                            </div>
                                          
                                        </div>
                                        <div class="clearfix"></div>

                                    
                                    
                                </div>
                            </div>
                            <?php } ?>
<div class="col-md-12 center">
                                        <div class="pagination pagination-sm">
                                            <li><?php echo $this->pagination->create_links(); ?></li>

                                        </div>  
                                    </div>
                        </div>
                    </div>
                </div>


                <!-- Project ends -->



            </div>
        </div>
    </div>
</div>   
