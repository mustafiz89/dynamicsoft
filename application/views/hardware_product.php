<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                 <div class="hero">
                        <!-- Title. Don't forget the <span> tag -->
                        <h3><span>Hardware Product</span></h3>
                        <!-- para -->
                        <p>Hardware Product List that we Sell with our Pos Software</p>
                     </div>
                <div class="row">
                    <div class="col-md-06">

                        <table class="table table-bordered table-striped">
                            <tr>
                                <th><h4>#</h4></th>
                                <th><h4>Model No</h4></th>
                                <th><h4>Specification</h4></th>
                                <th><h4>Price</h4></th>
                                <th><h4>Image</h4></th>
                            </tr>
                            <?php
                            $i = 0;
                            foreach ($get_all_data as $v_data) {
                                $i+=1;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $v_data->model ?></td>
                                    <td><p><?php echo $v_data->specification ?></p></td>
                                    <td><p style="color:green; font-size: 20px"><?php echo $v_data->price ?></p></td>
                                    <td><img src="<?php echo base_url() . $v_data->image ?>" height="150" width="150"/></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>

                </div>

            </div>
            <div class="col-md-12 center">
                <div class="pagination pagination-sm">
                    <li><?php echo $this->pagination->create_links(); ?></li>

                </div>  
            </div>
        </div>
    </div>
</div>
<style>
    h4{
        text-align: center;
        color: #0075b0;
    }
    
    </style>