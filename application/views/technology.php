
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            
            <!-- Support starts -->
            
            <div class="support">
               <div class="row">
                  <div class="col-md-12">
                  
                     <!-- Support -->
                     
                     <div class="row">
                        <div class="col-md-12 col-sm-8">
                           <div class="support-page">
                              <h4><?php echo $title;?></h4>
                              <p>
                                  <?php echo $get_data->description;?>
                              </p>
                              <h4>Tools And Framework</h4>
                              <!-- Lists -->
                                <ul id="slist">
                                    <!-- List #1 -->
                                    <li><i class="fa fa-plus-circle"></i>
                                       <!-- Title -->
                                       <a href="#">Programming Languages:</a>
                                       <!-- Para -->
                                       <p>ASP.NET, C#.NET, VB.NET, C++, Perl, Javascript, CSS, HTML, AJAX, JSP, PHP, CGI, J2SE, J2EE, J2ME, WAP, XML, XSL, XML Schema, XSD, XHTML, XPATH, SOAP, XML-RPC, WSDL, UDDI, SAX, DOM, JDOM, Xerces, Xalan, SAXON, VRML, Ruby.</p>
                                    </li>
                                    
                                    <li><i class="fa fa-plus-circle"></i>
                                       <!-- Title -->
                                       <a href="#">Framework:</a>
                                       <!-- Para -->
                                       <p>.Net, Spring, Zend, Codeigniter, bootstrap, JSON, JQuery, Backbone.js</p>
                                    </li>
                                                           
                               </ul>
                           
                              
                              <div class="clearfix"></div>                                
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                           
                          
                        </div>
                     </div>
                     
                  </div>
               </div>
            </div>
            
            
            <!-- FAQ ends -->
            
         </div>
      </div>
   </div>
</div>   
