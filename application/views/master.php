<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <!-- Title here -->
        <title>Dynamic Software Ltd</title>
        <!-- Description, Keywords and Author -->
        <meta name="description" content="Your description">
        <meta name="keywords" content="Your,Keywords">
        <meta name="author" content="ResponsiveWebInc">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <!-- Styles -->
        <!-- Bootstrap CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/settings.css" media="screen" />
        <!-- Pretty Photo -->
        <link href="<?php echo base_url(); ?>css/prettyPhoto.css" rel="stylesheet">
        <!-- Font awesome CSS -->
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">		
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <!-- Color Stylesheet - orange, blue, pink, brown, red or green-->
        <link href="<?php echo base_url(); ?>css/blue.css" rel="stylesheet"> 


        <!-- image gallery slider content -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/gallery_slider.css">

        <!-- End  image gallery slider content -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon/favicon.ico">
        <style>
            p{
                text-align: justify;
            }
        </style>
    </head>

    <body>

        <!-- Header starts -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-9 ">
                        <!-- Logo and site link -->
                        <div class="logo">
                            <a href="<?php echo base_url(); ?>welcome/index"><img src="<?php echo base_url(); ?>img/logo.png" height="60"/></a>
 <!--                            <h1><a href="<?php echo base_url(); ?>welcome/index"></a></h1>
                             <h4>Code Your Thought</h4>-->
                        </div>
                    </div>

                    <div class="col-md-3 ">

                        <div class="col-md-3">
                            <img src="<?php echo base_url(); ?>img/blue-mobile.jpg" height="25" width="25"/>
                        </div>
                        <div class="col-md-9">
                            <h2 style="margin-top:-4px; margin-left:-20px;">  01550-016312 </h2>
                        </div>
                        <div class="col-md-3">
                            <img src="<?php echo base_url(); ?>img/green-mobile.jpg" height="25" width="25"/>
                        </div>
                        <div class="col-md-9">
                            <h2 style="margin-top:-4px; margin-left:-20px;">01720-579057</h2>
                        </div>

                    </div>
                    <!--                    <div class="col-md-4 col-sm-4 col-sm-offset-4 col-md-offset-2">
                                            <div class="list">
                                                 Add your phone number here 
                                                <div class="phone">
                                                    <i class="fa fa-phone"></i> Phone: +8801720579057
                                                </div>
                                                <hr />
                                                 Add your email id here 
                                                <div class="email">
                                                    <i class="fa fa-envelope-o"></i> Email: info@dynamicsoftwareltd.com
                                                </div>
                                                <hr />
                                                 Add your address here 
                                                <div class="address">
                                                    <i class="fa fa-home"></i> Address: 144,Level-04, Motijheel C/A, Dhaka-1000.
                                                </div>
                                            </div>
                                        </div>-->
                </div>
            </div>
        </header>
        <!--/ Header ends -->

        <!-- Navigation Starts -->
        <!-- Note down the syntax before editing. It is the default twitter bootstrap navigation -->
        <div class="navbar bs-docs-nav" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
                    <!-- Navigation links starts here -->
                    <ul class="nav navbar-nav">
                        <li class="">
                            <a href="<?php echo base_url(); ?>welcome/index.aspx">Home</a>
                            <!-- Submenus -->							
                        </li>
                        <!-- Navigation with sub menu. Please note down the syntax before you need. Each and every link is important. -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a>
                            <!-- Submenus -->
                            <ul class="dropdown-menu">

                                <li><a href="<?php echo base_url(); ?>welcome/company_history.aspx">Company History</a></li>
                                <li><a href="<?php echo base_url(); ?>welcome/board_member.aspx">Board Member</a></li>
                                <li><a href="<?php echo base_url(); ?>welcome/developer_team.aspx">Developer Team</a></li>
                                <li><a href="<?php echo base_url(); ?>welcome/marketing_team.aspx">Marketing Team</a></li>



                            </ul>
                        </li> 
                        <li class="">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Product <b class="caret"></b></a>
                            <ul class="dropdown-menu">

                                <li><a href="<?php echo base_url(); ?>welcome/recent_product.aspx">Ready Product</a></li>                  
                                <li><a href="<?php echo base_url(); ?>welcome/upcoming_product.aspx">Upcoming Product</a></li>
                                <li><a href="<?php echo base_url(); ?>welcome/hardware_product.aspx">Hardware Product</a></li>

                            </ul>						
                        </li>
                        <li class="">
                            <a href="<?php echo base_url(); ?>welcome/service.aspx">Service</a>

                        </li>

                        <li class="">
                            <a href="<?php echo base_url(); ?>welcome/technology.aspx">Our Technology</a>
                        </li>

                        <li class="">
                            <a href="<?php echo base_url(); ?>welcome/client.aspx" >Our Client</a>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url(); ?>welcome/portfolio.aspx" >Portfolio</a>

                        </li>

<!--                        <li class="">
                            <a href="<?php echo base_url(); ?>welcome/blog.aspx" >Blog</a>

                        </li>-->




                        <li class="">
                            <a href="<?php echo base_url(); ?>welcome/contact.aspx">Contact</a>
                        </li> 
                    </ul>
                </nav>
            </div>
        </div>
        <!--/ Navigation Ends -->   


        <!-- Content strats -->




        <?php echo $mid_content; ?>








        <!--/ Content ends --> 

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <!-- Widget 3 -->
                        <div class="widget">
                            <h4>Social</h4>
                            <div class="social">
                                <a href="#"><i class="fa fa-facebook facebook"></i></a>
                                <a href="#"><i class="fa fa-linkedin linkedin"></i></a>
                                <a href="#"><i class="fa fa-twitter twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest pinterest"></i></a>                                
                                <a href="#"><i class="fa fa-google-plus google-plus"></i></a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <!-- Widget 1 -->
                        <div class="widget">
                            <h4>About Us</h4>
                            <!-- Social Media -->
                            <p>Dynamic Software Ltd is a Bangladesh based software company that focuses on highly qualitative, timely delivered and cost-effective all kind of software development. With a rich and varied experience in providing software development and project management capabilities and stringent quality standards ensure us to develop solutions that give your business an edge over your competitors. Our global software model makes sure we deliver maximum targeted result to YOU. </p>
                            
                            <p>Dynamic Software's customers benefit from our experience and superior service. Our company develops innovative products, technologies and services based on the needs of our customers.</p>

                        </div>
                    </div>
<!--                    <div class="col-md-4">
                         widget 2 
                        <div class="widget">
                            <h4>Recent project</h4>
                            <ul>
						  <li><i class="fa fa-angle-right"></i> <a href="#">Sed eu leo orci, in rhoncus puru</a></li>
                                   <li><i class="fa fa-angle-right"></i> <a href="#">Condimentum gravida metus</a></li>
                                   <li><i class="fa fa-angle-right"></i> <a href="#">Lpsum, in rhoncus purus</a></li>
                                   <li><i class="fa fa-angle-right"></i> <a href="#">Etiam at in rhoncus puru nul</a></li>
                                   <li><i class="fa fa-angle-right"></i> <a href="#">Fusce vel magnais dapibus facilisis</a></li>
                            </ul>
                        </div>
                    </div>-->

                </div>
                <div class="row">
                    <hr />
                    <div class="col-md-12"><p class="copy pull-left">
                            <!-- Copyright information. You can remove my site link. -->
                            Copyright &copy; Dynamic Software Ltd </p>
                        <p class="copy pull-right">Developed by <a href="#">Dynamic software Ltd</a></p>
                    </div>
                </div>
            </div>
        </footer>		
        <!--/ Footer -->

        <!-- Scroll to top -->
        <span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 

        <!-- Javascript files -->
        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>js/js/jquery.js"></script>
        <!-- Bootstrap JS -->
        <script src="<?php echo base_url(); ?>js/js/bootstrap.min.js"></script>
        <!-- Isotope, Pretty Photo JS -->
        <script src="<?php echo base_url(); ?>js/js/jquery.isotope.js"></script>
        <script src="<?php echo base_url(); ?>js/js/jquery.prettyPhoto.js"></script>
        <!-- Support Page Filter JS -->
        <script src="<?php echo base_url(); ?>js/js/filter.js"></script>
        <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script src="<?php echo base_url(); ?>js/js/jquery.themepunch.plugins.min.js"></script>
        <script src="<?php echo base_url(); ?>js/js/jquery.themepunch.revolution.min.js"></script>
        <!-- Respond JS for IE8 -->
        <script src="<?php echo base_url(); ?>js/js/respond.min.js"></script>
        <!-- HTML5 Support for IE -->
        <script src="<?php echo base_url(); ?>js/js/html5shiv.js"></script>
        <!-- Custom JS -->
        <script src="<?php echo base_url(); ?>js/js/custom.js"></script>


        <script>
            // Revolution Slider
            var revapi;
            jQuery(document).ready(function () {
                revapi = jQuery('.tp-banner').revolution(
                        {
                            delay: 9000,
                            startwidth: 1200,
                            startheight: 500,
                            hideThumbs: 200,
                            shadow: 3,
                            navigationType: "none",
                            hideThumbsOnMobile: "on",
                            hideArrowsOnMobile: "on",
                            hideThumbsUnderResoluition: 0,
                            touchenabled: "on",
                            fullWidth: "on"
                        });
            });
        </script>
    </body>	

</html>