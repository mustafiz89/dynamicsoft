

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- Contact starts -->

                <div class="contact">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- Contact hero -->
                            <div class="hero">
                               <!-- Title. Don't forget the <span> tag -->
                                <h3><span>Contact</span></h3>
                                <!-- para -->
                                <p></p> 
                            </div>
                            <!-- Contact -->

                            <div class="contact">
                                <div class="row">
                                      <div class="col-md-8">
                                            <div class="cwell">
                                                <!-- Google maps -->
                                                <div class="gmap">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.4802730171473!2d90.41659500000003!3d23.730247000000013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b85978349fcb%3A0xf8c454b311875293!2sDynamic+Software+Limited!5e0!3m2!1sbn!2sbd!4v1425363645999" width="600" height="250" frameborder="0" style="border:0"></iframe>                            
                                                </div>
                                                
                                            </div>
                                      </div>
                                          <div class="col-md-4">
                                               <div class="cwell">
                                                    <!-- Address section -->
                                                    <h3>Address</h3>
                                                     <div class="address">
                                                        <address>
                                                            <!-- Company name -->
                                                            <strong>Dynamic Software Ltd.</strong><br>
                                                            <!-- Address -->
                                                            144,Level-04, Motijheel C/A, Dhaka-1000.<br>
                                                            <!-- Phone number -->
                                                            Phone:+8801720579057.
                                                            <p>Email: info@dynamicsoftwareltd.com.</p>
                                                        </address>
                                                     </div>
                                                    </div>
                                                </div>
                                        </div>

                                   
                                
                                <div class="row">

                                    <div class="col-md-8 col-sm-6">
                                        <div class="cwell">
                                            <!-- Contact form -->
                                            <h5>Contact Form</h5>
                                            <div style="color:green; font-size: 16px">
                                                <?php
                                                $msg = $this->session->userdata('message');
                                                if ($msg) {
                                                    echo $msg;
                                                    $this->session->unset_userdata('message');
                                                }
                                                ?>
                                            </div>
                                            <hr />
                                            <div class="form">
                                                <!-- Contact form (not working)-->
                                                <form class="form-horizontal" action="<?php echo base_url();?>welcome/send_mail" method="post">
                                                    <!-- Name -->
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3" for="name">Name</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" required id="name" name="name">
                                                        </div>
                                                    </div>
                                                    <!-- Email -->
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3" for="email">Email</label>
                                                        <div class="col-md-9">
                                                            <input type="email" class="form-control" required="email" id="email" name="email">
                                                        </div>
                                                    </div>                                                 
                                                    
                                                    <!-- Comment -->
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3" for="comment">Comment</label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control" id="comment" required  rows="3" name="comment"></textarea>
                                                        </div>
                                                    </div>
                                                    <!-- Buttons -->
                                                    <div class="form-group">
                                                        <!-- Buttons -->
                                                        <div class="col-md-9 col-md-offset-3">
                                                            <button type="submit" class="btn btn-default">Submit</button>
                                                            <button type="reset" class="btn btn-default">Reset</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <hr />

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>


                <!-- Service ends -->


            </div>
        </div>
    </div>
</div>   
