
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            
            <!-- Clients starts -->
            
            <div class="clients-page">
               <div class="row">
                  <div class="col-md-12">
                  
                     <!-- clients hero -->
                     <div class="hero">
                        <!-- Title. Don't forget the <span> tag -->
                        <h3><span>Our Clients</span></h3>
                        <!-- para -->
                        <p>Some of Our Prospective Client list is given in below</p>
                     </div>
                     <!-- Clients -->
                     
                     <!-- Clients list. Note down the class name before editing. -->
                     <div class="row">
                         <?php
                         foreach($get_all_data as $v_data)
                         {
                         ?>
                            <div class="col-md-6 col-sm-6">
                           <div class="c_client">
                              <!-- Client image -->
                              <div class="c_image">
                                 <img src="<?php echo base_url().$v_data->image;?>" alt="" height="100" width="100"/>
                              </div>
                              <div class="c_text">
                                  <p><?php echo $v_data->description?></p>
                              </div>
                                <div class="button pull-right"><a href="<?php echo $v_data->weblink?>">Visit Site</a></div> 
                              
                              
                           </div>
                        </div>
                     
                     
                      <?php 
                         }
                      ?>
                     </div>
                     
                       <div class="col-md-12 center">
                         <div class="pagination pagination-sm">
                             <li><?php echo $this->pagination->create_links(); ?></li>

                         </div>  
                     </div>
                  </div>
               </div>
            </div>
            
            
            <!-- Service ends -->
            
           
            
         </div>
      </div>
   </div>
</div>   
