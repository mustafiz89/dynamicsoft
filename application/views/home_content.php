<style>
    p{
        text-align: justify;
    }
</style>


<div class="content">
    <div class="container">

        <!-- Slider Starts -->
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>	<!-- SLIDE  -->
                                        <li data-transition="flyin" data-slotamount="4" data-masterspeed="300">
                                             
                                            <img src="<?php echo base_url(); ?>img/slider/slide1.jpg"  alt=""  data-bgposition="left top" data-duration="2000" data-bgfit="200" data-bgfitend="100" />
                                         
                                        
                                        </li>
                                         
                                        <li data-transition="cube" data-slotamount="4" data-masterspeed="300">
                                            
                                            <img src="<?php echo base_url(); ?>img/slider/slide2.jpg"  alt=""  data-bgposition="left top" data-duration="2000" data-bgfit="200" data-bgfitend="100" />
                    
                                        </li>
                                        
                                        <li data-transition="square" data-slotamount="4" data-masterspeed="300">
                                            
                                            <img src="<?php echo base_url(); ?>img/slider/slide3.jpg"  alt=""  data-bgposition="left top" data-duration="2000" data-bgfit="200" data-bgfitend="100" />
                    
                                        </li>
                 

                </ul>
            </div>
        </div>
        <!--/ Slider End -->

        <div class="bor"></div>

        <div class="main-content">
            <div class="row">
                <div class="col-md-7">
                    <h2><?php echo $get_data->title;?></h2>
<!--                    <p class="main-meta">Leave for the far of grammar</p>-->
                    <p><?php echo $get_data->description;?></p></div>
                <div class="col-md-5">
                    <div class="main-box">
                        <h4>Why Choose us </h4>
                        <p> Over the years, we have successfully guided our clients through the technology potholes and blind turns along the way. We know and we believe that we are different from rest, in the way we interact with you, our approach to problem solving, and above all – our attitude </p>
                        <p>Several factors make us different from other offshore companies and make us an ideal development partner:</p>
                        <ul>
                            <li><i class="fa fa-angle-right"></i>Technical expertise </li>
                            <li><i class="fa fa-angle-right"></i> Affordable price</li>
                            <li><i class="fa fa-angle-right"></i> Guarantee of Work Performance</li>
                        </ul>
                        <!--                        <div class="button">
                                                    <a href="#"><i class="fa fa-shopping-cart"></i> Buy Now</a> <a href="#"><i class="fa fa-magic"></i> Try Now</a>
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="bor"></div>


        <!--         Features list. Note down the class name before editing. 
                <div class="row">
                   <h4 style="text-align: center;">Our Service</h4><br>
                    <div class="col-md-3 col-sm-6">
                        
                        <div class="afeature">
                            <div class="afmatter">
                                <i class="fa fa-windows"></i>
                                
                                <h4>Web Application Development</h4>
                                <p></p>
                            </div>
                        </div>
                    </div>
                   <div class="col-md-3 col-sm-6">
                        <div class="afeature">
                            <div class="afmatter">
                                <i class="fa fa-user"></i>
                                <h4>Website Design & Development</h4>
                                <p></p>
                            </div>
                        </div>                        
                    </div>   
                   
                    <div class="col-md-3 col-sm-6">
                        <div class="afeature">
                            <div class="afmatter">
                                <i class="fa fa-desktop"></i>
                                <h4>Desktop Software Development</h4>
                                <p><a>Click Here</a></p>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="afeature">
                            <div class="afmatter">
                                <i class="fa fa-cloud"></i>
                                <h4>Domain & Hosting</h4>
                                <p></p>
                            </div>
                        </div>                        
                    </div>
                                         
                </div>
        
                 Features ends -->


        <!-- Testimonials -->

        <!-- Recent post starts. Note down the class name before editing. -->

        <div class="rposts">
            <h4>Our Clients</h4>
            <div class="row">

                <div id="slider1_container" class="gcontainer">


                    <div u="slides" class="gslider_image">
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/Abob.png" /></div>
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/Bangladesh-Guides.png" /></div>
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/Bangladesh-Police.png" /></div>
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/Biz-Index.png" /></div>
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/Jobs-On-BD.png" /></div>
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/LGMC.png" /></div>
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/Meherunnessa.png" /></div>
                        <div><img u="image" src="<?php echo base_url(); ?>image/client/Misbahul.png" /></div>

                    </div>

                    <span u="arrowleft" class="jssora03l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
                    </span>

                    <span u="arrowright" class="jssora03r" style="width: 55px; height: 55px; top: 123px; right: 8px">
                    </span>


                </div>

            </div>

        </div>

        <!-- Recent post ends -->            
        <!--        <div class="bor"></div>
        
        
                 Clients starts 
        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="clients">
                                    <img class="img-responsive" src="img/clients/1.png" alt="" />
                                    <img class="img-responsive" src="img/clients/2.png" alt="" />
                                    <img class="img-responsive" src="img/clients/3.png" alt="" />
                                    <img class="img-responsive" src="img/clients/4.png" alt="" />
                                    <img class="img-responsive" src="img/clients/5.png" alt="" />
                                    <img class="img-responsive" src="img/clients/6.png" alt="" />
                                </div>
                            </div>
                        </div>-->

        <!-- Clients ends -->           

    </div>
</div>   
<script  src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js"></script>
<script  src="<?php echo base_url(); ?>js/jssor.js"></script>
<script  src="<?php echo base_url(); ?>js/jssor.slider.js"></script>
<script  src="<?php echo base_url(); ?>js/gallery_slider.js"></script>