<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            
            <!-- Team starts -->
            
            <div class="about">
               <div class="row">
                  <div class="col-md-12">
                  
                     <!-- Team hero -->
                     <div class="hero">
                        <!-- Title. Don't forget the <span> tag -->
                        <h3><span><?php echo $title;?></span></h3>
                        <!-- para -->
                     </div>
                     <!-- Our team -->
                     
                     <!-- Teams -->
                     
                     <div class="teams">
                        <div class="row">
                            <?php
                              foreach($get_all_data as $v_data)
                              {
                              
                              ?>
                           <div class="col-md-4 col-sm-3 col-xs-6">
                              <!-- Staff #1 -->
                              
                              <div class="staff">
                                 <!-- Picture -->
                                 <div class="pic">
                                    <img src="<?php echo base_url().$v_data->image;?>" alt="" class="img-responsive" />
                                 </div>
                                 <!-- Details -->
                                 <div class="details">
                                    <!-- Name and designation -->
                                    <div class="desig pull-left">
                                       <h3 class="name"><?php echo $v_data->name;?></h3>
                                       <h4><?php echo $v_data->designation;?></h4>
                                    </div>
                                    <!-- Social media details. Replace # with profile links -->
                                    <div class="asocial pull-right">
                                       <a href="#"><i class="fa fa-facebook"></i></a>
                                       <a href="#"><i class="fa fa-twitter"></i></a>
                                       <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div>
                                 <hr />
                              </div>
                              <?php 
                              
                              }
                              ?>
                           </div>
                          
                          
                           </div>
                          
                        </div>
                        
                                        
                     </div>
                     
                  </div>
               </div>
            </div>
            
            <!-- About ends -->
            
            
            
         </div>
      </div>
   </div>
</div>   
