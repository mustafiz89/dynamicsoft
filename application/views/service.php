<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- Service starts -->

                <div class="servic">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- Service hero -->
                            <div class="hero">
                               <!-- Title. Don't forget the <span> tag -->
                                <h3><span>Service</span></h3>
                                <!-- para -->
                                <p>
                                    We Provide Many service in our web and desktop application section.
                                </p>
                            </div>
                            <!-- Service -->

                            <!-- Service list. Note down the class name before editing. -->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Service 1. Class name - "serv-a" -->


                                    <?php
                                    foreach($get_all_data as $v_data){
                                        ?>
                                        <div class="serv-a">
                                            <div class="serv">
                                                <div class="simg">
                                                    <!-- Font awesome icon. -->
                                                    <img src="<?php echo base_url().$v_data->image; ?>" width="450" height="250"/>
                                                </div>
                                                <!-- Service title -->
                                                <h4><?php echo $v_data->title?></h4>
                                                <!-- Service para -->
                                                <p style="text-align: justify">
                                                  <?php echo $v_data->short_description?>

                                                    <a href="<?php echo base_url();?>welcome/service_details/<?php echo $v_data->id;?>">.....Read More</a>

                                                </p></div>
                                        </div>

                                        <?php
                                    }
                                    ?>

                                    <div class="clearfix"></div>
                                </div>   

                                <!--                        <div class="col-md-6">
                                                        
                                                            Service 3. Class name - "serv-a" 
                                                           <div class="serv-a">
                                                              <div class="serv">
                                                                 <div class="simg">
                                                                    <i class="fa fa-bullhorn"></i>
                                                                 </div>
                                                                 <h4>WordPress Theme</h4>
                                                                 <p>Fusce imperdiet, risus eget viverra faucibus, diam mi vestibulum libero, tellus magna nec enim. Nunc dapibus varius interdum.</p>
                                                              </div>
                                                           </div>
                                                           
                                                            Service 4. Class name - "serv-b" 
                                                           <div class="serv-b">
                                                              <div class="serv">
                                                                 <div class="simg">
                                                                    <i class="fa fa-cloud"></i>
                                                                 </div>
                                                                 <h4>Mobile Apps</h4>
                                                                 <p>Fusce imperdiet, risus eget viverra faucibus, diam mi vestibulum libero, tellus magna nec enim. Nunc dapibus varius interdum.</p>
                                                              </div>
                                                           </div>
                                                           
                                                           
                                                           
                                                           
                                                           <div class="clearfix"></div>                        
                                                        </div>-->
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Service ends -->

            </div>
        </div>
    </div>
</div>   
