<?php 
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {    
    
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
            
        }
       
    }
    public function manage_recent_product()
    {
        $data=array();
        $data['title']='Recent Product';
        $this->load->library('pagination');
        $config['base_url'] = base_url() .'product/manage_recent_product';        
        $config['total_rows'] = $this->db->count_all('tbl_recent_product');
        $config['per_page'] = '10';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
        
        
        $config['prev_link'] = 'Prev';
        $config['next_link'] = 'Next ';
        
        $this->pagination->initialize($config);
        $data['get_all_data']=$this->product_model->select_all_recent_product($config['per_page'], $this->uri->segment(3));
        $data['admin_mid_content']=$this->load->view('admin/recent_product',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_recent_product()
    {
         $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['short_description'] = $this->input->post('short_description', true);
        $data['long_description'] = $this->input->post('long_description', true);
        $data['license'] = $this->input->post('license', true);
        $data['product_size'] = $this->input->post('product_size', true);
        $data['price'] = $this->input->post('price', true);
        $data['used_technology'] = $this->input->post('used_technology', true);
        /* upload Image */
        $config['upload_path'] = 'image/recent_product/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('product/manage_recent_product');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->product_model->save_recent_product($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_recent_product'); 
    }
    
     public function edit_recent_product($id)
    {
        $data=array();
        $data['title']='Edit Product Information';
        $data['get_data']=$this->product_model->select_recent_product_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_recent_product',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_recent_product()
    {
        $data = array();
        $id=$this->input->post('id',true);
        $data['name'] = $this->input->post('name', true);
        $data['short_description'] = $this->input->post('short_description', true);
        $data['long_description'] = $this->input->post('long_description', true);
        $data['license'] = $this->input->post('license', true);
        $data['product_size'] = $this->input->post('product_size', true);
        $data['price'] = $this->input->post('price', true);
        $data['used_technology'] = $this->input->post('used_technology', true);
        $db_image=$this->product_model->select_recent_product_by_id($id);
        /* upload Image */
        $config['upload_path'] = 'image/recent_product/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload', $config); 
        if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('product/edit_recent_product/' . $id);
            }
        } else {
            $fdata = $this->upload->data(); 
            
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }
        
//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->product_model->update_recent_product($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_recent_product');
    }
    
    public function delete_recent_product($id)// End upcoming Product Controller
    {
       $db_image=$this->product_model->select_recent_product_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->product_model->delete_recent_product($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_recent_product');   
        
    }
    
    
    public function manage_upcoming_product()
    {
        $data=array();
        $data['title']='Upcoming Product';
        $data['get_all_data']=$this->product_model->select_all_upcoming_product();
        $data['admin_mid_content']=$this->load->view('admin/upcoming_product',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_upcoming_product()
    {
         $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['description'] = $this->input->post('description', true);
        $data['possible_price'] = $this->input->post('possible_price', true);
        $data['used_tools'] = $this->input->post('used_tools', true);
        $data['release_date'] = $this->input->post('release_date', true);
       
        /* upload Image */
        $config['upload_path'] = 'image/upcoming_product/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('product/manage_upcoming_product');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->product_model->save_upcoming_product($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_upcoming_product'); 
    }
    
     public function edit_upcoming_product($id)
    {
        $data=array();
        $data['title']='Edit Upcoming Product Information';
        $data['get_data']=$this->product_model->select_upcoming_product_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_upcoming_product',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_upcoming_product()
    {
        $data = array();
        $id=$this->input->post('id',true);
        $data['name'] = $this->input->post('name', true);
        $data['description'] = $this->input->post('description', true);
        $data['possible_price'] = $this->input->post('possible_price', true);
        $data['used_tools'] = $this->input->post('used_tools', true);
        $data['release_date'] = $this->input->post('release_date', true);
       
        $db_image=$this->product_model->select_upcoming_product_by_id($id);
        /* upload Image */
        $config['upload_path'] = 'image/upcoming_product/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload', $config); 
        if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('product/edit_upcoming_product/' . $id);
            }
        } else {
            $fdata = $this->upload->data(); 
            
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }
        
//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->product_model->update_upcoming_product($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_upcoming_product');
    }
    
    public function delete_upcoming_product($id)// End upcoming Product Controller
    {
       $db_image=$this->product_model->select_upcoming_product_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->product_model->delete_upcoming_product($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_upcoming_product');   
        
    }
    
    public function manage_hardware_product()
    {
        $data=array();
        $data['title']='Hardware Product';
        $this->load->library('pagination');
        $config['base_url'] = base_url() .'product/manage_hardware_product';        
        $config['total_rows'] = $this->db->count_all('tbl_hardware_product');
        $config['per_page'] = '10';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
        
        
        $config['prev_link'] = 'Prev';
        $config['next_link'] = 'Next ';
        
        $this->pagination->initialize($config);
        $data['get_all_data']=$this->product_model->select_all_hardware_product($config['per_page'], $this->uri->segment(3));
        $data['admin_mid_content']=$this->load->view('admin/hardware_product',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
      public function add_hardware_product()
    {
         $data = array();
        $data['model'] = $this->input->post('model', true);
        $data['specification'] = $this->input->post('specification', true);
        $data['price'] = $this->input->post('price', true);
       
        /* upload Image */
        $config['upload_path'] = 'image/hardware_product/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '2000';
        $config['max_width'] = '800';
        $config['max_height'] = '600';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('product/manage_hardware_product');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->product_model->save_hardware_product($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_hardware_product'); 
    }
    
     public function edit_hardware_product($id)
    {
        $data=array();
        $data['title']='Edit Hardware Product Information';
        $data['get_data']=$this->product_model->select_hardware_product_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_hardware_product',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_hardware_product()
    {
        $data = array();
        $id=$this->input->post('id',true);
        $data['model'] = $this->input->post('model', true);
        $data['specification'] = $this->input->post('specification', true);
        $data['price'] = $this->input->post('price', true);
        $db_image=$this->product_model->select_hardware_product_by_id($id);
        /* upload Image */
        $config['upload_path'] = 'image/hardware_product/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '2000';
        $config['max_width'] = '800';
        $config['max_height'] = '600';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload', $config); 
        if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('product/edit_hardware_product/' . $id);
            }
        } else {
            $fdata = $this->upload->data(); 
            
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }
        
//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->product_model->update_hardware_product($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_hardware_product');
    }
    
    public function delete_hardware_product($id)// End hardware Product Controller
    {
       $db_image=$this->product_model->select_hardware_product_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->product_model->delete_hardware_product($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('product/manage_hardware_product');   
        
    }
    
    
}