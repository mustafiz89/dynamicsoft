<?php 
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {    
    
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
            
        }
       
    }
    
    public function index()//Board Member Controller 
    {
        $data=array();
        $data['title']='Board Member';
        $data['get_all_data']=$this->about_model->select_all_board_member();
        $data['admin_mid_content']=$this->load->view('admin/board_member',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_board_member()
    {
       $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['designation'] = $this->input->post('designation', true);
        /* upload Image */
        $config['upload_path'] = 'image/board_member/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('about/index');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->about_model->save_board_member($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/index'); 
    }
    
    public function edit_board_member($id)
    {
        $data=array();
        $data['title']='Edit Member Information';
        $data['get_data']=$this->about_model->select_member_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_board_member',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    public function update_board_member()
    {
        $data = array();
        $id=$this->input->post('id',true);
        $data['name'] = $this->input->post('name', true);
        $data['designation'] = $this->input->post('designation', true);
        $db_image=$this->about_model->select_member_by_id($id);
        
        /* upload Image */
        $config['upload_path'] = 'image/board_member/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
       
        if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('about/edit_board_member/' . $id);
            }
        }
        else {
            $fdata = $this->upload->data();     
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->about_model->update_board_member($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/index');
        
    }
    
    public function delete_board_member($id)
    {
        $db_image=$this->about_model->select_member_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->about_model->delete_board_member($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/index');   
        
    }
    
    
    public function manage_programmer_team()//Programmer Controller
    {
        $data=array();
        $data['title']='Our Programmer Team';
        $data['get_all_data']=$this->about_model->select_all_programmer();
        $data['admin_mid_content']=$this->load->view('admin/programmer_team',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_programmer()
    {
        $data = array();
        
        $data['name'] = $this->input->post('name', true);
        $data['designation'] = $this->input->post('designation', true);
        $data['contact'] = $this->input->post('contact', true);
        $data['email'] = $this->input->post('email', true);
        /* upload Image */
        $config['upload_path'] = 'image/programmer_team/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('about/manage_programmer_team');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->about_model->save_programmer($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/manage_programmer_team'); 
    }
    
    public function edit_programmer($id)
    {
        $data=array();
        $data['title']='Edit Programmer Information';
        $data['get_data']=$this->about_model->select_programmer_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_programmer',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_programmer()
    {
         $data = array();
         $id=$this->input->post('id',true);
        $data['name'] = $this->input->post('name', true);
        $data['designation'] = $this->input->post('designation', true);
        $data['contact'] = $this->input->post('contact', true);
        $data['email'] = $this->input->post('email', true);
        $db_image=$this->about_model->select_programmer_by_id($id);
        /* upload Image */
        $config['upload_path'] = 'image/programmer_team/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        
            
       if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('about/edit_programmer/' . $id);
            }
        } else {
            $fdata = $this->upload->data(); 
            
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }
        
//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->about_model->update_programmer($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/manage_programmer_team');
    }
    
    public function delete_programmer($id)//End Programmer Controller
    {
        $db_image=$this->about_model->select_programmer_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->about_model->delete_programmer($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/manage_programmer_team');   
        
    }
    
    
    public function manage_marketing_team()//Marketing  Controller
    {
        $data=array();
        $data['title']='Our Marketing Team';
        $data['get_all_data']=$this->about_model->select_all_marketing();
        $data['admin_mid_content']=$this->load->view('admin/marketing_team',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_marketing()
    {
        $data = array();
        
        $data['name'] = $this->input->post('name', true);
        $data['designation'] = $this->input->post('designation', true);
        $data['division'] = $this->input->post('division', true);
        $data['contact'] = $this->input->post('contact', true);
        $data['email'] = $this->input->post('email', true);
        /* upload Image */
        $config['upload_path'] = 'image/marketing_team/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('about/manage_marketing_team');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->about_model->save_marketing($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/manage_marketing_team'); 
    }
    
    public function edit_marketing($id)
    {
        $data=array();
        $data['title']='Edit marketing Information';
        $data['get_data']=$this->about_model->select_marketing_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_marketing_team',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_marketing()
    {
         $data = array();
         $id=$this->input->post('id',true);
        $data['name'] = $this->input->post('name', true);
        $data['designation'] = $this->input->post('designation', true);
        $data['division'] = $this->input->post('division', true);
        $data['contact'] = $this->input->post('contact', true);
        $data['email'] = $this->input->post('email', true);
        $db_image=$this->about_model->select_marketing_by_id($id);
        /* upload Image */
        $config['upload_path'] = 'image/marketing_team/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        
            
       if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('about/edit_marketing/' . $id);
            }
        } else {
            $fdata = $this->upload->data(); 
            
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }
        
//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->about_model->update_marketing($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/manage_marketing_team');
    }
    
    public function delete_marketing($id)//End Marketing Controller
    {
        $db_image=$this->about_model->select_marketing_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->about_model->delete_marketing($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('about/manage_marketing_team');   
        
    }
}