<?php 
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Administrator extends CI_Controller {    
    
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
        }
    }

	
	public function index()
	{
            $data=array();
            $data['title']='Dashboard'; 
            $data['admin_mid_content']=$this->load->view('admin/dashboard',$data,true);
            $this->load->view('admin/admin_master',$data);
           
	}
        
        public function logout()
        {
            $this->session->unset_userdata('admin_id');
            $this->session->unset_userdata('admin_name');
            $sdata=array();
            $sdata['message']='You are successfully Logout';
            $this->session->set_userdata($sdata);
            redirect('login','refresh');
            
        }
        
         public function dashboard()
        {
            $data=array();
            $data['title']='Dashboard';
            $data['admin_mid_content']=$this->load->view('admin/dashboard',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        public function welcome_message()
        {
            $data=array();
            $data['title']="Welcome Message";
            $data['get_data']=$this->administrator_model->select_welcome_message_by_id(1);
            $data['admin_mid_content']=$this->load->view('admin/welcome_message',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        
        public function update_welcome_message()
        {
            $data=array();
            $id=$this->input->post('id',true);
            $data['title']=$this->input->post('title',true);
            $data['description']=$this->input->post('description',true);
            $this->administrator_model->update_welcome_message($id,$data);
            $sdata=array();
            $sdata['message']="Update Successfully";
            $this->session->set_userdata($sdata); 
            redirect('administrator/welcome_message');
        }
        
        
        public function company_history()
        {
            $data=array();
            $data['title']="Company History";
            $data['get_data']=$this->administrator_model->select_history_by_id(1);
            $data['admin_mid_content']=$this->load->view('admin/company_history',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
         public function update_company_history()
        {
            $data=array();
            $id=$this->input->post('id',true);
            $data['title']=$this->input->post('title',true);
            $data['description']=$this->input->post('description',true);
            $this->administrator_model->update_history($id,$data);
            $sdata=array();
            $sdata['message']="Update Successfully";
            $this->session->set_userdata($sdata); 
            redirect('administrator/company_history');
        } 
        
        public function manage_technology()// Technology Controller
        {
            $data=array();
            $data['title']='Our Technology ';
            $data['get_data']=$this->administrator_model->select_technology_by_id(1);
            $data['admin_mid_content']=$this->load->view('admin/technology',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        
        public function update_technology()
        {
            $data=array();
            $id=$this->input->post('id',true);
            $data['description']=$this->input->post('description',true);
            $this->administrator_model->update_technology($id,$data);
            $sdata=array();
            $sdata['message']="Update Successfully";
            $this->session->set_userdata($sdata); 
            redirect('administrator/manage_technology');
        }
        
        public function manage_portfolio()
        {
            $data = array();
            $data['title'] = 'Portfolio ';
            $this->load->library('pagination');
            $config['base_url'] = base_url() . 'adminsitrator/manage_porfolio';
            $config['total_rows'] = $this->db->count_all('tbl_portfolio');
            $config['per_page'] = '10';
            $config['cur_tag_open'] = '<a><b>';
            $config['cur_tag_close'] = '</b></a>';

            $config['prev_link'] = 'Prev';
            $config['next_link'] = 'Next ';

            $this->pagination->initialize($config);
            $data['get_all_data'] = $this->administrator_model->select_all_portfolio($config['per_page'], $this->uri->segment(3));
            $data['admin_mid_content'] = $this->load->view('admin/portfolio', $data, true);
            $this->load->view('admin/admin_master', $data);
        }
        
        public function save_portfolio()
        {
        $config['upload_path'] = 'image/portfolio/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('administrator/manage_portfolio');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->administrator_model->save_portfolio($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_portfolio'); 
        }
        
        public function delete_portfolio($id)
        {
            $db_image=$this->administrator_model->select_portfolio_by_id($id);
            if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
            $this->administrator_model->delete_portfolio_by_id($id);
            $sdata = array();
            $sdata['d_message'] = 'Delete Successfully';
            $this->session->set_userdata($sdata);
            redirect('administrator/manage_portfolio');   
        }
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>