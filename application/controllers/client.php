<?php

session_start();
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Client extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $admin_id = $this->session->userdata('admin_id');
        if ($admin_id == null) {
            redirect('login', 'refresh');
        }
    }

    public function index() {
        $data = array();
        $data['title'] = 'Our Client';
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'client/index';
        $config['total_rows'] = $this->db->count_all('tbl_client');
        $config['per_page'] = '10';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
//        $config['first_tag_open'] = '<li>';
//        $config['first_tag_close'] = '</li>';
//        $config['last_tag_open'] = '<li>';
//        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = ' Prev';
        $config['next_link'] = 'Next ';
//        $config['next_tag_open'] = '<li>';
//        $config['next_tag_close'] = '</li>';
//        $config['prev_tag_open'] = '<li>';
//        $config['prev_tag_close'] = '</li>';
//        $config['num_tag_open'] = '<li>';
//        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['get_all_data']=$this->client_model->select_all_client($config['per_page'], $this->uri->segment(3));
        $data['admin_mid_content'] = $this->load->view('admin/manage_client', $data, true);
        $this->load->view('admin/admin_master', $data);
    }

    public function add_client() {
        $data = array();
        $data['description'] = $this->input->post('description', true);
        $data['weblink'] = $this->input->post('weblink', true);
        /* upload Image */
        $config['upload_path'] = 'image/client/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '1000';
        $config['max_width'] = '200';
        $config['max_height'] = '150';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('client/index');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->client_model->save_client_info($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('client/index');
    }
    
    public function edit_client($id)
    {
        $data=array();
        $data['title']='Edit Client Info';
        $data['get_data']=$this->client_model->select_client_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_client',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    public function update_client()
    {
        $data=array();
        $id=$this->input->post('id',true);
        $data['weblink']=$this->input->post('weblink',true);
        $data['description']=$this->input->post('description',true);
        $db_image=$this->client_model->select_client_by_id($id);
        
         $config['upload_path'] = 'image/client/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '1000';
        $config['max_width'] = '200';
        $config['max_height'] = '150';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        
       
       if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('product/edit_recent_product/' . $id);
            }
        } else {
            $fdata = $this->upload->data(); 
            
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->client_model->update_client_info($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('client/index');
        
    }
     public function delete_client($id)
    {
        $db_image=$this->client_model->select_client_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->client_model->delete_client_by_id($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('client/index');   
        
    }

}

?>