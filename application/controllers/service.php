<?php 
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends CI_Controller {    
    
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
            
        }
       
    }
    
     public function manage_service()
    {
         $data=array();
        $data['title']='Our Service';
        
        $this->load->library('pagination');
        $config['base_url'] = base_url() .'service/manage_service';        
        $config['total_rows'] = $this->db->count_all('tbl_service');
        $config['per_page'] = '1';
        $config['cur_tag_open'] = '<a><b>';
        $config['cur_tag_close'] = '</b></a>';
        
        
        $config['prev_link'] = 'Prev';
        $config['next_link'] = 'Next ';
        
        $this->pagination->initialize($config);
        $data['get_all_data']=$this->service_model->select_all_service($config['per_page'], $this->uri->segment(3));
        $data['admin_mid_content']=$this->load->view('admin/service',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_service()
    {
         $data = array();
        $data['title'] = $this->input->post('title', true);
        $data['short_description'] = $this->input->post('short_description', true);
        $data['long_description'] = $this->input->post('long_description', true);
       
        /* upload Image */
        $config['upload_path'] = 'image/service/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $sdata = array();
            $sdata['message'] = $error;
            $this->session->set_userdata($sdata);
            redirect('service/manage_service');
        } else {
            $fdata = $this->upload->data();
            $data['image'] = $config['upload_path'] . $fdata['file_name'];
        }

//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->service_model->save_service($data);
        $sdata = array();
        $sdata['message'] = 'Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('service/manage_service'); 
    }
    
     public function edit_service($id)
    {
        $data=array();
        $data['title']='Edit service Information';
        $data['get_data']=$this->service_model->select_service_by_id($id);
        $data['admin_mid_content']=$this->load->view('admin/edit_service',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_service()
    {
        $data = array();
        $id=$this->input->post('id',true);
        $data['title'] = $this->input->post('title', true);
        $data['short_description'] = $this->input->post('short_description', true);
        $data['long_description'] = $this->input->post('long_description', true);
       
        $db_image=$this->service_model->select_service_by_id($id);
        /* upload Image */
        $config['upload_path'] = 'image/service/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = '3000';
        $config['max_width'] = '1200';
        $config['max_height'] = '900';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload', $config); 
        if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {

                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('service/edit_service/' . $id);
            }
        } else {
            $fdata = $this->upload->data(); 
            
            if ($fdata['file_name'] == '') {
                return;
            }
             else{  
                  if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
             }
        }
        
//            echo '<pre>';
//            print_r($data);
//            exit;
        
        $this->service_model->update_service($id,$data);
        $sdata = array();
        $sdata['d_message'] = 'Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('service/manage_service');
    }
    
    public function delete_service($id)// End upcoming Product Controller
    {
       $db_image=$this->service_model->select_service_by_id($id);
         if($db_image->image)
            {
                $image=$db_image->image;
                unlink($image);        
            }
        $this->service_model->delete_service($id);
        $sdata = array();
        $sdata['d_message'] = 'Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('service/manage_service');   
        
    }
    
    
    
}
