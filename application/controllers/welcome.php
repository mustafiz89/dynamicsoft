<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            $data=array();
            $data['title']='Home';
            $data['get_data']=$this->welcome_model->select_welcome_message(1);
            $data['mid_content']=$this->load->view('home_content',$data,true);
            $this->load->view('master',$data);
	}
        
        public function company_history()
        {
            $data=array();
            $data['title']='Company History';
            $data['get_data']=$this->welcome_model->select_company_history(1);
//            echo '<pre>';
//            print_r($data['get_data']);
//            exit;
            $data['mid_content']=$this->load->view('company_history',$data,true);
            $this->load->view('master',$data);
        }
        
        public function board_member()
        {
            $data=array();
            $data['title']='Board Member';
            $data['get_all_data']=$this->welcome_model->select_all_board_member();
            $data['mid_content']=$this->load->view('board_member',$data,true);
            $this->load->view('master',$data);
        }
         public function developer_team()
        {
            $data=array();
            $data['title']='Programmer Team';
             $data['get_all_data']=$this->welcome_model->select_all_programmer();
            $data['mid_content']=$this->load->view('developer_team',$data,true);
            $this->load->view('master',$data);
        }
         public function marketing_team()
        {
            $data=array();
            $data['title']='Marketing Team';
             $data['get_all_data']=$this->welcome_model->select_all_marketing_person();
            $data['mid_content']=$this->load->view('marketing_team',$data,true);
            $this->load->view('master',$data);
        }
        
        public function recent_product()
        {
            $data=array();
            $data['title']='Ready product';
            $this->load->library('pagination');
            $config['base_url'] = base_url() .'welcome/recent_product';        
            $config['total_rows'] = $this->db->count_all('tbl_recent_product');
            $config['per_page'] = '2';
            $config['cur_tag_open'] = '<a><b>';
            $config['cur_tag_close'] = '</b></a>';


            $config['prev_link'] = 'Prev';
            $config['next_link'] = 'Next ';
        
            $this->pagination->initialize($config);
            $data['get_all_data']=$this->product_model->select_all_recent_product($config['per_page'], $this->uri->segment(3));     
                         
            $data['mid_content']=$this->load->view('recent_product',$data,true);
            $this->load->view('master',$data);
        }
        
        
        public function product_details($id)
        {
            $data=array();
            $data['title']='Product Details';
            $data['get_data']=$this->welcome_model->select_product_by_id($id);
            
            $data['mid_content']=$this->load->view('product_details',$data,true);
            $this->load->view('master',$data);
        }
        public function upcoming_product()
        {
            $data=array();
            $data['title']='Upcoming_product';
            $this->load->library('pagination');
            $config['base_url'] = base_url() .'welcome/upcoming_product';        
            $config['total_rows'] = $this->db->count_all('tbl_upcoming_product');
            $config['per_page'] = '2';
            $config['cur_tag_open'] = '<a><b>';
            $config['cur_tag_close'] = '</b></a>';


            $config['prev_link'] = 'Prev';
            $config['next_link'] = 'Next ';
        
            $this->pagination->initialize($config);
            $data['get_all_data']=$this->product_model->select_all_upcoming_product($config['per_page'], $this->uri->segment(3));
            $data['mid_content']=$this->load->view('upcoming_product',$data,true);
            $this->load->view('master',$data);  
        }
         public function hardware_product()
        {
            $data=array();
            $data['title']='Hardware Product';
            $this->load->library('pagination');
            $config['base_url'] = base_url() .'welcome/hardware_product';        
            $config['total_rows'] = $this->db->count_all('tbl_hardware_product');
            $config['per_page'] = '10';
            $config['cur_tag_open'] = '<a><b>';
            $config['cur_tag_close'] = '</b></a>';


            $config['prev_link'] = 'Prev';
            $config['next_link'] = 'Next ';
        
            $this->pagination->initialize($config);
            $data['get_all_data']=$this->product_model->select_all_hardware_product($config['per_page'], $this->uri->segment(3));     
                         
            $data['mid_content']=$this->load->view('hardware_product',$data,true);
            $this->load->view('master',$data);
        }
        public function service()
        {
            $data=array();
            $data['title']='Our Service';
            $data['get_all_data']=$this->welcome_model->select_all_service();
            $data['mid_content']=$this->load->view('service',$data,true);
            $this->load->view('master',$data);  
        }
        public function service_details($id)
        {
            $data=array();
            $data['title']='Service Details';
            $data['get_data']=$this->welcome_model->select_service_by_id($id);
            $data['mid_content']=$this->load->view('service_details',$data,true);
            $this->load->view('master',$data);  
        }
        public function technology()
        {
            $data=array();
            $data['title']='Our Technology';
            $data['get_data']=$this->welcome_model->select_technology_by_id(1);
            $data['mid_content']=$this->load->view('technology',$data,true);
            $this->load->view('master',$data);  
        }
        public function client()
        {
            $data=array();
            $data['title']="Our Client's";
            $this->load->library('pagination');
            $config['base_url'] = base_url() .'welcome/client';        
            $config['total_rows'] = $this->db->count_all('tbl_client');
            $config['per_page'] = '12';
            $config['cur_tag_open'] = '<a><b>';
            $config['cur_tag_close'] = '</b></a>';


            $config['prev_link'] = 'Prev';
            $config['next_link'] = 'Next ';
        
            $this->pagination->initialize($config);
            $data['get_all_data']=$this->welcome_model->select_all_client($config['per_page'], $this->uri->segment(3));
            $data['mid_content']=$this->load->view('client',$data,true);
            $this->load->view('master',$data);  
        }
        public function portfolio()
        {
            $data=array();
            $data['title']='Our Portfolio';
            $data['get_all_data']=$this->welcome_model->select_all_portfolio();
            $data['mid_content']=$this->load->view('portfolio',$data,true);
            $this->load->view('master',$data);  
        }
        public function blog()
        {
            $data=array();
            $data['title']='Blog';
            $data['mid_content']=$this->load->view('blog',$data,true);
            $this->load->view('master',$data);  
        }
        public function contact()
        {
            $data=array();
            $data['title']='Contact Us';
            $data['mid_content']=$this->load->view('contact',$data,true);
            $this->load->view('master',$data);  
        }
        public function send_mail()
        {
            $mdata=array();
            $mdata['from_address']="info@dynamicsoftwareltd.com";            
            $mdata['to_address']="sohag2882@yahoo.com";
            $mdata['name']=$this->input->post('name',true);            
            $mdata['email']=$this->input->post('email',true);
            $mdata['subject']="Give information To U";
            $mdata['comment']=$this->input->post('comment',true);
            
//            echo '<pre>';
//            print_r($mdata);
//            exit();
//            
            //$this->mailer_model->send_email($mdata);
            $sdata=array();
            $sdata['message']='Message Send Successfully';
            $this->session->set_userdata($sdta);            
            redirect('welcome/contact');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */