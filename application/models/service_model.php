<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Service_model extends CI_Model{
    
    
    public function select_all_service($per_page,$offset)// Service Model
    {
        if($offset==null)
       {
           $offset=0;
       }
    
        $this->db->select('*');
        $this->db->from('tbl_service');
         $this->db->limit($per_page,$offset);         
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function save_service($data)
    {
        $this->db->insert('tbl_service',$data);
    }
    public function select_service_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function update_service($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_service',$data);
    }
    public function delete_service($id)//End service Model
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_service');
    }
    
}
