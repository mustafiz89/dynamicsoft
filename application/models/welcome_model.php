<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Welcome_model extends CI_Model {
    
    public function select_welcome_message($id)
    {
       $this->db->select('*');
       $this->db->from('tbl_welcome_message');
       $this->db->where('id',$id);
       $query_result=$this->db->get();
       $result=$query_result->row();
       return $result; 
    }
    public function select_company_history($id)
    {
       $this->db->select('*');
       $this->db->from('tbl_company_history');
       $this->db->where('id',$id);
       $query_result=$this->db->get();
       $result=$query_result->row();
       return $result; 
    }
    public function select_all_board_member()
    {
       $this->db->select('*');
       $this->db->from('tbl_board_member');      
       $query_result=$this->db->get();
       $result=$query_result->result();
       return $result; 
    }
    
    public function select_all_programmer()
    {
       $this->db->select('*');
       $this->db->from('tbl_programmer_team');      
       $query_result=$this->db->get();
       $result=$query_result->result();
       return $result;  
    }
    public function select_all_marketing_person()
    {
        $this->db->select('*');
        $this->db->from('tbl_marketing_team');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_all_recent_product($per_page, $offset)
    {
        if($offset==null)
        {
            $offset=0;
        }
        $this->db->select('*');
        $this->db->from('tbl_recent_product');
        $this->db->limit($per_page, $offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;  
    }
    
    public function select_product_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_recent_product');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function select_all_upcoming_product($per_page, $offset)
    {
        if($offset==null)
        {
            $offset=0;
        }
        $this->db->select('*');
        $this->db->from('tbl_recent_product');
        $this->db->limit($per_page, $offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;  
    }
    public function select_all_hardware_product($per_page, $offset)
    {
        if($offset==null)
        {
            $offset=0;
        }
        $this->db->select('*');
        $this->db->from('tbl_hardware_product');
        $this->db->limit($per_page, $offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;  
    }
    
    public function select_all_service()
    {
        
        $this->db->select('*');
        $this->db->from('tbl_service');
        
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;  
    }
    public function select_service_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
     public function select_technology_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_technology');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function select_all_client($per_page, $offset)
    {
         if($offset==null)
        {
            $offset=0;
        }
        $this->db->select('*');
        $this->db->from('tbl_client');
        $this->db->limit($per_page, $offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;  
    }
    public function select_all_portfolio()
    {
        $this->db->select('*');
        $this->db->from('tbl_portfolio');
        
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;  
    }
}