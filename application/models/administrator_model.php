<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Administrator_model extends CI_Model{
    
   public function select_welcome_message_by_id($id)
   {
       $this->db->select('*');
       $this->db->from('tbl_welcome_message');
       $this->db->where('id',$id);
       $query_result=$this->db->get();
       $result=$query_result->row();
       return $result;
   }
   public function update_welcome_message($id,$data)
   {
     $this->db->where('id',$id);
     $this->db->update('tbl_welcome_message',$data);
   }
   
    public function select_history_by_id($id)
   {
       $this->db->select('*');
       $this->db->from('tbl_company_history');
       $this->db->where('id',$id);
       $query_result=$this->db->get();
       $result=$query_result->row();
       return $result;
   }
   public function update_history($id,$data)
   {
     $this->db->where('id',$id);
     $this->db->update('tbl_company_history',$data);
   }
   
   
    public function select_technology_by_id($id)// Technology Model
   {
       $this->db->select('*');
       $this->db->from('tbl_technology');
       $this->db->where('id',$id);
       $query_result=$this->db->get();
       $result=$query_result->row();
       return $result;
   }
   public function update_technology($id,$data)
   {
     $this->db->where('id',$id);
     $this->db->update('tbl_technology',$data);
   }
   
   public function select_all_portfolio($per_page, $offset)// Portfolio Model 
   {
        if($offset==null)
       {
           $offset=0;
       }
       $this->db->select('*');
       $this->db->from('tbl_portfolio');
       $this->db->limit($per_page,$offset);
       $query_result=$this->db->get();
       $result=$query_result->result();
       return $result;
   }
    public function save_portfolio($data)
    {
        $this->db->insert('tbl_portfolio',$data);
    }
   public function select_portfolio_by_id($id)
   {
       $this->db->select('*');
       $this->db->from('tbl_portfolio');
       $this->db->where('id',$id);
       $query_result=$this->db->get();
       $result=$query_result->row();
       return $result;
   }
   
   public function delete_portfolio_by_id($id)// End Portfolio Model 
   {
     $this->db->where('id',$id);
     $this->db->delete('tbl_portfolio');
   }
}
?>