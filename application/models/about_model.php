<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class About_model extends CI_Model {

    public function select_all_board_member() //Board Member Model
    {
        $this->db->select('*');
        $this->db->from('tbl_board_member');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function save_board_member($data)
    {
        $this->db->insert('tbl_board_member',$data);
    }
    public function select_member_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_board_member');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function update_board_member($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_board_member',$data);
    }
    public function delete_board_member($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_board_member');
    }
    
    public function select_all_programmer()// Programmer Model
    {
        $this->db->select('*');
        $this->db->from('tbl_programmer_team');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_programmer_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_programmer_team');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
     public function save_programmer($data)
    {
        $this->db->insert('tbl_programmer_team',$data);
    }
     public function update_programmer($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_programmer_team',$data);
    }
    public function delete_programmer($id)// Programmer Model End
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_programmer_team ');
    }
    
    
    public function select_all_marketing()// Marketing Model
    {
        $this->db->select('*');
        $this->db->from('tbl_marketing_team');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_marketing_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_marketing_team');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
     public function save_marketing($data)
    {
        $this->db->insert('tbl_marketing_team',$data);
    }
     public function update_marketing($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_marketing_team',$data);
    }
    public function delete_marketing($id)// Marketing Model End
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_marketing_team ');
    }
    
    
    
}
