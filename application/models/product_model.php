<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Product_model extends CI_Model{
    
    
    public function select_all_recent_product($per_page,$offset)// Recent Product Model
    {
        if($offset==null)
       {
           $offset=0;
       }
    
        $this->db->select('*');
        $this->db->from('tbl_recent_product');
         $this->db->limit($per_page,$offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function save_recent_product($data)
    {
        $this->db->insert('tbl_recent_product',$data);
    }
    public function select_recent_product_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_recent_product');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function update_recent_product($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_recent_product',$data);
    }
    public function delete_recent_product($id)//End Recent Product Model
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_recent_product');
    }
    
    
      public function select_all_upcoming_product()// Recent Product Model
    {
           
        $this->db->select('*');
        $this->db->from('tbl_upcoming_product');
         //$this->db->limit($per_page,$offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function save_upcoming_product($data)
    {
        $this->db->insert('tbl_upcoming_product',$data);
    }
    public function select_upcoming_product_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_upcoming_product');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function update_upcoming_product($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_upcoming_product',$data);
    }
    public function delete_upcoming_product($id)//End Recent Product Model
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_upcoming_product');
    }
    
     public function select_all_hardware_product($per_page,$offset)// Hardware Product Model
    {
        if($offset==null)
       {
           $offset=0;
       }
    
        $this->db->select('*');
        $this->db->from('tbl_hardware_product');
         $this->db->limit($per_page,$offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function save_hardware_product($data)
    {
        $this->db->insert('tbl_hardware_product',$data);
    }
    public function select_hardware_product_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_hardware_product');
        $this->db->where('id',$id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function update_hardware_product($id, $data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_hardware_product',$data);
    }
    public function delete_hardware_product($id)//End hardware Product Model
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_hardware_product');
    }
    
}