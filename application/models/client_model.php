<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Client_model extends CI_Model{
    
    public function save_client_info($data)
    {
        $this->db->insert('tbl_client',$data);
    }
    
    public function select_all_client($per_page,$offset)        
    {
          if($offset==null)
       {
           $offset=0;
       }
        $this->db->select('*');
        $this->db->from('tbl_client');
        $this->db->limit($per_page,$offset);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    
    public function select_client_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_client');
        $this->db->where('id',$id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function update_client_info($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->update('tbl_client',$data);
    }
    public function delete_client_by_id($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_client');
    }
    
}
?>